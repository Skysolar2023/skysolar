<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth', 'isAdmin'])->group(function () {
    Route::get('/', [\App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('admin.dashboard');

    Route::get('/logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('admin.user.logout');

    //CATEGORIES
    Route::middleware(['can:Manage Pages'])->group(function () {
        Route::get('/categories/draw', [\App\Http\Controllers\Admin\CategoryController::class, 'draw'])->name('admin.categories.draw');
        Route::get('/categories/search', [\App\Http\Controllers\Admin\CategoryController::class, 'search'])->name('admin.categories.search');
        Route::get('/categories/list', [\App\Http\Controllers\Admin\CategoryController::class, 'list'])->name('admin.category.list');
        Route::post('/categories/bulk-actions', [\App\Http\Controllers\Admin\CategoryController::class, 'bulkActions']);
        Route::resource('categories', \App\Http\Controllers\Admin\CategoryController::class);
    });

    //Page Contents
    Route::middleware(['can:Manage Pages'])->group(function () {
        Route::get('/page-contents/draw', [\App\Http\Controllers\Admin\PageContentController::class, 'draw'])->name('admin.page-contents.draw');
        Route::get('/page-contents/search', [\App\Http\Controllers\Admin\PageContentController::class, 'search'])->name('admin.page-contents.search');
        Route::get('/page-contents/list', [\App\Http\Controllers\Admin\PageContentController::class, 'list'])->name('admin.page-contents.list');
        Route::post('/page-contents/bulk-actions', [\App\Http\Controllers\Admin\PageContentController::class, 'bulkActions']);
        Route::resource('page-contents', \App\Http\Controllers\Admin\PageContentController::class);
    });

    //Page Contents
    Route::middleware(['can:Manage Pages'])->group(function () {
        Route::get('/form-submissions/draw', [\App\Http\Controllers\Admin\FormSubmissionController::class, 'draw'])->name('admin.form-submissions.draw');
        Route::get('/form-submissions/search', [\App\Http\Controllers\Admin\FormSubmissionController::class, 'search'])->name('admin.form-submissions.search');
        Route::get('/form-submissions/list', [\App\Http\Controllers\Admin\FormSubmissionController::class, 'list'])->name('admin.form-submissions.list');
        Route::get('/form-submissions/{id}/downloadCv', [\App\Http\Controllers\Admin\FormSubmissionController::class, 'downloadCv'])->name('admin.form-submissions.downloadCv');
        Route::post('/form-submissions/bulk-actions', [\App\Http\Controllers\Admin\FormSubmissionController::class, 'bulkActions']);
        Route::resource('form-submissions', \App\Http\Controllers\Admin\FormSubmissionController::class);
    });

    //Page Contents
    Route::middleware(['can:Manage Pages'])->group(function () {
        Route::get('/blog/draw', [\App\Http\Controllers\Admin\BlogController::class, 'draw'])->name('admin.blog.draw');
        Route::get('/blog/search', [\App\Http\Controllers\Admin\BlogController::class, 'search'])->name('admin.blog.search');
        Route::get('/blog/list', [\App\Http\Controllers\Admin\BlogController::class, 'list'])->name('admin.blog.list');
        Route::post('/blog/bulk-actions', [\App\Http\Controllers\Admin\BlogController::class, 'bulkActions']);
        Route::resource('blog', \App\Http\Controllers\Admin\BlogController::class);
    });

    //Page Contents
    Route::middleware(['can:Manage Pages'])->group(function () {
        Route::get('/categories/draw', [\App\Http\Controllers\Admin\CategoryController::class, 'draw'])->name('admin.categories.draw');
        Route::get('/categories/search', [\App\Http\Controllers\Admin\CategoryController::class, 'search'])->name('admin.categories.search');
        Route::get('/categories/list', [\App\Http\Controllers\Admin\CategoryController::class, 'list'])->name('admin.categories.list');
        Route::post('/categories/bulk-actions', [\App\Http\Controllers\Admin\CategoryController::class, 'bulkActions']);
        Route::resource('categories', \App\Http\Controllers\Admin\CategoryController::class);
    });

    //Page Contents
    Route::middleware(['can:Manage Pages'])->group(function () {
        Route::get('/examples/draw', [\App\Http\Controllers\Admin\ExampleController::class, 'draw'])->name('admin.examples.draw');
        Route::get('/examples/search', [\App\Http\Controllers\Admin\ExampleController::class, 'search'])->name('admin.examples.search');
        Route::get('/examples/list', [\App\Http\Controllers\Admin\ExampleController::class, 'list'])->name('admin.examples.list');
        Route::post('/examples/bulk-actions', [\App\Http\Controllers\Admin\ExampleController::class, 'bulkActions']);
        Route::resource('examples', \App\Http\Controllers\Admin\ExampleController::class);
    });

    //Page Contents
    Route::middleware(['can:Manage Pages'])->group(function () {
        Route::get('/categories-example/draw', [\App\Http\Controllers\Admin\CategoryExampleController::class, 'draw'])->name('admin.categories-example.draw');
        Route::get('/categories-example/search', [\App\Http\Controllers\Admin\CategoryExampleController::class, 'search'])->name('admin.categories-example.search');
        Route::get('/categories-example/list', [\App\Http\Controllers\Admin\CategoryExampleController::class, 'list'])->name('admin.categories-example.list');
        Route::post('/categories-example/bulk-actions', [\App\Http\Controllers\Admin\CategoryExampleController::class, 'bulkActions']);
        Route::resource('categories-example', \App\Http\Controllers\Admin\CategoryExampleController::class);
    });

    //Settings
    Route::middleware(['can:Manage Pages'])->group(function () {
        Route::get('settings/general', function () { return view('admin.settings.general'); })->name('settings.general');
        Route::resource('settings', \App\Http\Controllers\Admin\SettingController::class);
    });

    //ATTACHMENTS
    Route::middleware(['can:Manage Attachments'])->group(function () {
        Route::resource('attachments', \App\Http\Controllers\Admin\AttachmentController::class);
    });


    Route::post('/upload-file', [\App\Http\Controllers\Admin\MediaController::class, 'uploadFile'])->name('media.uploadFile');
    Route::post('/slugify', [\App\Http\Controllers\Admin\DashboardController::class, 'slugify'])->name('text.slugify');
});
