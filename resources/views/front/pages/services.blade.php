@extends('layouts.front')
@section('content')

    <section class="services-section-1">
        <div class="services-text-hidden">
            <p class="services-sec-1-text">OUR VISION FOR CUSTOMERS:</p>
        </div>
        <div class="services-text-hidden">
            <div class="services-text-and-image">
                <p class="services-sec-1-text">TO BECOME</p>
                <div class="services-text-hidden">
                    <img class="services-sec-1-img" src="{{ asset('images/static/image 13.webp') }}" alt="">
                </div>
                <div class="services-text-hidden">
                    <p class="services-sec-1-text services-pos-left">100% ENERGY</p>
                </div>
            </div>
        </div>
        <div class="services-text-hidden services-dis-flex">
            <p class="services-sec-1-text">INDEPENDENT</p>
            <div class="services-text-hidden">
                <p class="services-sec-1-text-s">Since 2008, Solar Optimum has paved the way for premium solar, battery storage
                    and
                    roofing solutions for residential and commercial customers in California.</p>
            </div>
        </div>
    </section>
    <section class="services-section-1-tablet">
        <div class="services-text-hidden">
            <p class="services-sec-1-text">OUR VISION FOR CUSTOMERS:</p>
        </div>
        <div class="services-text-hidden">
            <img class="services-sec-1-img" src="{{ asset('images/static/image 13.webp') }}" alt="">
        </div>
        <div class="services-text-hidden">
            <div class="services-marg-left">
                <p class="services-sec-1-text">TO BECOME 100% ENERGY</p>
            </div>
        </div>
        <div class="services-text-hidden">
            <p class="services-sec-1-text">INDEPENDENT</p>
        </div>
        <div class="services-text-hidden">
            <p class="services-sec-1-text-s">Since 2008, Solar Optimum has paved the way for premium solar, battery storage
                and
                roofing solutions for residential and commercial customers in California.</p>
        </div>
    </section>

    <section class="services-section-1-mobile">
        <div class="services-text-hidden">
            <p class="services-sec-1-text">OUR VISION FOR</p>
        </div>
        <div class="services-text-hidden">
            <p class="services-sec-1-text">CUSTOMERS:</p>
        </div>
        <div class="services-text-hidden">
            <img class="services-sec-1-img" src="{{ asset('images/static/image 13.webp') }}" alt="">
        </div>
        <div class="services-text-hidden">
            <div class="services-marg-left">
                <p class="services-sec-1-text">TO BECOME</p>
            </div>
        </div>
        <div class="services-text-hidden">
            <p class="services-sec-1-text">100% ENERGY</p>
        </div>
        <div class="services-text-hidden">
            <p class="services-sec-1-text">INDEPENDENT</p>
        </div>
        <div class="services-text-hidden">
            <p class="services-sec-1-text-s">Since 2008, Solar Optimum has paved the way for premium solar, battery storage
                and
                roofing solutions for residential and commercial customers in California.</p>
        </div>
    </section>
    <section class="services-section-2">
        <div class="services-wrapper">
            <div class="services-panel">
                <div class="services-text-cont-1">
                    <div class="services-text-hidden">
                        <h3 class="services-sec-2-h3">Converting sunlight</h3>
                    </div>
                </div>
                <div class="services-text-cont-2">
                    <div class="services-text-hidden">
                        <h3 class="services-sec-2-h3"> into gold</h3>
                    </div>
                </div>
                <div class="services-sec-2-img-text-cont">
                    <div class="services-img-cont">
                        <img class="services-sec-2-img" src="{{ asset('images/static/Group 1000004503.webp') }}" alt="">
                    </div>
                    <div class="services-sec-2-text-cont-2">
                        <div class="services-sec-2-text-p-cont">
                            <p class="services-sec-2-text">Solar Panel Installation:</p>
                            <p class="services-sec-2-text-2">Our team of experts can install solar panels on your rooftop,
                                ground, or any other appropriate location to maximize your solar potential.</p>
                        </div>
                        <div class="services-sec-2-text-p-cont-2">
                            <p class="services-sec-2-text">Solar Maintenance and Repair:</p>
                            <p class="services-sec-2-text-2"> Our team provides regular maintenance services to ensure
                                that your solar panels are always functioning at optimal levels. We also offer repair
                                services in case of any damage or malfunctioning.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="services-panel services-panel-2">
                <div class="services-panel-2-text-cont">
                    <div class="services-sec-2-text-p-cont-1">
                        <p class="services-sec-2-text">Solar Consultancy:</p>
                        <p class="services-sec-2-text-2">We offer solar consultancy services to help you determine the best solar
                            solutions for your specific needs. Our experts can provide information on the latest solar
                            technologies, government incentives, and financing options.</p>
                    </div>
                    <div class="services-sec-2-text-p-cont-2">
                        <p class="services-sec-2-text">Solar Energy Monitoring:</p>
                        <p class="services-sec-2-text-2">We provide solar energy monitoring services that help you track your
                            energy
                            consumption and production. This service helps you optimize your energy usage and identify
                            any
                            issues with your solar system.</p>
                    </div>
                </div>
                <div class="services-panel-2-img-cont">
                    <img class="services-panel-2-img" src="{{ asset('images/static/Group 1000004505(2).webp') }}" alt="">
                </div>
            </div>
            <div class="services-panel services-panel-3">
                <div class="services-panel-3-img-cont">
                    <img class="services-panel-3-img" src="{{ asset('images/static/image 11.webp') }}" alt="">
                </div>
                <div class="services-panel-3-text-cont">
                    <div class="services-panel-3-text-p-cont">
                        <p class="services-sec-2-text">Solar System Design:</p>
                        <p class="services-sec-2-text-2">Our team of engineers can design a customized solar system that meets
                            your
                            specific energy needs. We take into account your energy usage patterns, available space, and
                            other factors to design a system that maximizes your solar potential.</p>
                    </div>
                    <div class="services-sec-2-text-p-cont-2">
                        <p class="services-sec-2-text">Solar Financing:</p>
                        <p class="services-sec-2-text-2">We offer various financing options to make solar energy more affordable
                            and accessible. Our financing options include lease, power purchase agreements (PPA), and
                            loans.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="services-section-3">
        <div class="services-sec-3-text-cont">
            <div class="services-text-hidden">
                <h3 class="services-sec-3-h3">ROOFING</h3>
            </div>
            <p class="services-sec-3-p">Whatever your roof looks like, you can go solar and we can make it happen.Whether you
                have asphalt shingles, metal panels, clay tiles, or any other type of roofing material, our team of
                experts has the knowledge and experience to design and install a solar panel system that will work
                seamlessly with your roof.</p>
        </div>
        <div class="services-sec-3-img-cont">
            <img class="services-sec-3-img" src="{{ asset('images/static/Rectangle 21.webp') }}" alt="">
        </div>
    </section>
    <section class="services-section-4">
        <div class="services-sec-4-text-cont">
            <div class="services-text-hidden">
                <h3 class="services-sec-4-h3">BATTERIES</h3>
            </div>
            <p class="services-sec-4-p-2">Our battery storage solutions allow you to access electricity even when the sun isn't
                shining, providing an efficient way to store excess energy generated by solar panels. They are suitable
                for homes, businesses, and other facilities, and offer a high-quality, self-sufficient energy option.
                Special programs may be available for those living in fire hazard zones, with up to three free batteries
                and government rebates. The batteries provide peace of mind during power outages, particularly for those
                with medical equipment that needs electricity.</p>
        </div>
        <div class="services-sec-4-img-cont">
            <img class="services-sec-4-img" src="{{ asset('images/static/image 9(2).webp') }}" alt="">
        </div>
    </section>
    <section class="services-section_5">
        <div class="services-sec-5-text-cont">
            <div class="services-text-hidden">
                <div class="services-sec-5-h3-cont">
                    <h3 class="services-sec-5-h3">Get a</h3>
                    <h3 class="services-sec-5-h3 services-sec-5-h3-color-blue">30%</h3>
                    <h3 class="services-sec-5-h3">Federal Tax Credit</h3>
                </div>
            </div>
            <div class="services-text-hidden">
                <h3 class="services-sec-5-h3">with Our Solar Battery</h3>
            </div>
            <div class="services-text-hidden">
                <div class="services-sec-5-h3-p-cont">
                    <h3 class="services-sec-5-h3">Solutions</h3>
                    <p class="services-sec-5-h3-p">We'll assess eligibility and charge only for installation and city fees to
                        transition to sustainable energy with federal tax credit.</p>
                </div>
            </div>
        </div>
        <div class="services-sec-5-text-cont-tablet">
            <div class="services-text-hidden">
                <div class="services-sec-5-h3-cont">
                    <h3 class="services-sec-5-h3">Get a</h3>
                    <h3 class="services-sec-5-h3 sec-5-h3-color-blue">30%</h3>
                    <h3 class="services-sec-5-h3">Federal Tax</h3>
                </div>
            </div>
            <div class="services-text-hidden">
                <h3 class="services-sec-5-h3">Credit with Our Solar</h3>
            </div>
            <div class="services-text-hidden">
                <h3 class="services-sec-5-h3">Battery Solutions</h3>
            </div>
            <p class="services-sec-5-h3-p">We'll assess eligibility and charge only for installation and city fees to
                transition to sustainable energy with federal tax credit.</p>
        </div>
        <div class="services-sec-5-text-cont-mobile">
            <div class="services-text-hidden">
                <div class="services-sec-5-h3-cont">
                    <div class="services-text-hidden">
                        <h3 class="services-sec-5-h3-2">Get a</h3>
                    </div>
                    <div class="services-text-hidden">
                        <h3 class="services-sec-5-h3-2 sec-5-h3-color-blue">30%</h3>
                    </div>
                </div>
                <div class="services-text-hidden">
                    <h3 class="services-sec-5-h3-2">Federal Tax</h3>
                </div>
            </div>
            <div class="services-text-hidden">
                <h3 class="services-sec-5-h3-2">Credit with</h3>
            </div>
            <div class="services-text-hidden">
                <h3 class="services-sec-5-h3-2">Our Solar</h3>
            </div>
            <div class="services-text-hidden">
                <h3 class="services-sec-5-h3-2">Battery</h3>
            </div>
            <div class="services-text-hidden">
                <h3 class="services-sec-5-h3-2">Solutions</h3>
            </div>
            <p class="services-sec-5-h3-p">We'll assess eligibility and charge only for installation and city fees to
                transition to sustainable energy with federal tax credit.</p>
        </div>
        <div class="services-sec_12_info">
            <div class="services-sec_12_text_cont">
                <div class="services-text-hidden">
                    <h5 class="services-sec_12_h5">Lorem ipsum dolor sit</h5>
                </div>
                <div class="services-text-hidden">
                    <h5 class="services-sec_12_h5">amet consectetur.</h5>
                </div>
            </div>
            <div class="services-sec_12_text_cont_tablet">
                <div class="services-text-hidden">
                    <h5 class="services-sec_12_h5">Lorem ipsum dolor sit amet</h5>
                </div>
                <div class="services-text-hidden">
                    <h5 class="services-sec_12_h5">consectetur.</h5>
                </div>
            </div>
            <div class="services-sec_12_text_cont_mobile">
                <div class="services-text-hidden">
                    <h5 class="services-sec_12_h5-2">Lorem ipsum dolor</h5>
                </div>
                <div class="services-text-hidden">
                    <h5 class="services-sec_12_h5-2">sit amet</h5>
                </div>
                <div class="services-text-hidden">
                    <h5 class="services-sec_12_h5-2">consectetur.</h5>
                </div>
            </div>
            <p class="services-sec_12_p">Lorem ipsum dolor sit amet consectetur. Blandit fermentum praesen</p>
            <div class="services-cont_input">
                <input class="services-sec_12_input" type="text" name="" id="" placeholder="Full name">
            </div>
            <div class="services-cont_input_2">
                <input class="services-sec_12_input" type="email" name="" id="" placeholder="Email address">
            </div>
            <div class="services-cont_input_3">
                <input class="services-sec_12_input" type="tel" name="" id="" placeholder="Phone number">
            </div>
            <div class="services-cont_input_4">
                <textarea class="services-sec_12_input_2" type="text" name="" id="" placeholder="Message"></textarea>
            </div>
            <button class="services-sec_12_btn">Submit</button>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{ asset('js/gsap-3.11.4-ScrollTrigger.js') }}"></script>
    <script src="{{ asset('js/gsap-3.11.4.js') }}"></script>
    <script src="{{ asset('js/front/service.js') }}"></script>
@endsection
