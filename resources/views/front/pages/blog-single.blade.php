@extends('layouts.front')

@section('content')
    <section class="blog-single-section-1">
        <div class="blog-single-cont-img">
            <img class="blog-single-sec-1-img" src="{{ $blog->mainImage->urls['large'] }}" alt="">
        </div>
        <div class="blog-single-sec-1-text-cont">
            <div class="blog-single-sec-1-text-cont-pc">
                <div class="blog-single-h3-color-cont">
                    <div class="blog-single-text-hidden">
                        <h3 class="blog-single-sec-1-h3-color-yellow">{{ $blog->title }}</h3>
                    </div>
                    <div class="blog-single-text-hidden">
{{--                        <h3 class="blog-single-sec-1-h3">dolor sit amet</h3>--}}
                    </div>
                </div>
                <div class="blog-single-text-hidden">
                    <h3 class="blog-single-sec-1-h3">consectetur. Blandit</h3>
                </div>
                <div class="blog-single-text-hidden">
                    <h3 class="blog-single-sec-1-h3">fermentum praesen</h3>
                </div>
            </div>
            <div class="blog-single-sec-1-text-cont-mobile">
                <div class="blog-single-text-hidden">
                    <h3 class="sec-1-h3-color-yellow">Lorem ipsum</h3>
                </div>
                <div class="blog-single-text-hidden">
                    <h3 class="blog-single-sec-1-h3">dolor sit amet</h3>
                </div>
                <div class="blog-single-text-hidden">
                    <h3 class="blog-single-sec-1-h3">consectetur.</h3>
                </div>
                <div class="blog-single-text-hidden">
                    <h3 class="blog-single-sec-1-h3">Blandit</h3>
                </div>
                <div class="blog-single-text-hidden">
                    <h3 class="blog-single-sec-1-h3">fermentum</h3>
                </div>
                <div class="blog-single-text-hidden">
                    <h3 class="blog-single-sec-1-h3">praesen</h3>
                </div>
            </div>
            <div class="blog-single-icon-text-cont">
                <div class="blog-single-icon-text-cont-1">
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M12.5 22C18.0228 22 22.5 17.5228 22.5 12C22.5 6.47715 18.0228 2 12.5 2C6.97715 2 2.5 6.47715 2.5 12C2.5 17.5228 6.97715 22 12.5 22Z"
                            stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                        <path d="M12.5 6V12L16.5 14" stroke="white" stroke-width="2" stroke-linecap="round"
                              stroke-linejoin="round" />
                    </svg>
                    <p class="blog-single-icon-text">10 min read</p>
                </div>
                <div class="blog-single-icon-text-cont-2">
                    <p class="blog-single-icon-text">Anna Smith</p>
                    <img src="{{ asset('images/static/Ellipse 199.webp') }}" alt="">
                </div>
            </div>
        </div>
    </section>
    <section class="blog-single-section-2">
        {!! $blog->content !!}
{{--        <div class="blog-single-text-hidden">--}}
{{--            <h3 class="blog-single-sec-2-h3">Our blog.dolor sit </h3>--}}
{{--        </div>--}}
{{--        <div class="blog-single-text-hidden">--}}
{{--            <h3 class="blog-single-sec-2-h3">lorem ipsum</h3>--}}
{{--        </div>--}}
{{--        <div class="blog-single-svg-img-cont">--}}
{{--            <div class="blog-single-icons">--}}
{{--                <svg width="34" height="34" viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
{{--                    <rect width="34" height="34" fill="#1931AF" />--}}
{{--                    <path--}}
{{--                        d="M18.9982 26.4077V17.7028H21.2219L21.5166 14.7031H18.9982L19.002 13.2017C19.002 12.4193 19.0708 12.0001 20.1107 12.0001H21.5009V9H19.2769C16.6054 9 15.6652 10.4552 15.6652 12.9024V14.7034H14V17.7031H15.6652V26.4077H18.9982Z"--}}
{{--                        fill="white" />--}}
{{--                </svg>--}}
{{--                <svg width="34" height="34" viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
{{--                    <rect width="34" height="34" fill="#1931AF" />--}}
{{--                    <path--}}
{{--                        d="M14.3389 20.2548L14.0666 24.3333C14.4666 24.3333 14.6434 24.1504 14.8666 23.9333L16.7884 22.0646L20.7866 25.0185C21.5224 25.4245 22.0535 25.2143 22.2368 24.3354L24.8614 11.967C25.1301 10.8896 24.4507 10.401 23.7468 10.7206L8.33484 16.6634C7.28283 17.0853 7.28896 17.6722 8.14308 17.9338L12.0981 19.1682L21.2545 13.3915C21.6868 13.1294 22.0835 13.2703 21.7579 13.5593L14.3386 20.2547L14.3389 20.2548Z"--}}
{{--                        fill="white" />--}}
{{--                </svg>--}}
{{--                <svg width="34" height="34" viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
{{--                    <rect width="34" height="34" fill="#1931AF" />--}}
{{--                    <path--}}
{{--                        d="M17.5208 15.0046L17.5544 15.5582L16.9948 15.4905C14.9579 15.2306 13.1784 14.3493 11.6676 12.8692L10.9289 12.1347L10.7387 12.6771C10.3358 13.886 10.5932 15.1628 11.4325 16.0215C11.8802 16.496 11.7795 16.5638 11.0073 16.2814C10.7387 16.191 10.5036 16.1232 10.4812 16.1571C10.4029 16.2362 10.6715 17.2643 10.8841 17.6711C11.1751 18.236 11.7683 18.7897 12.4174 19.1173L12.9658 19.3772L12.3167 19.3885C11.6899 19.3885 11.6676 19.3998 11.7347 19.6371C11.9585 20.3715 12.8427 21.1511 13.8276 21.4901L14.5214 21.7273L13.9171 22.0889C13.0218 22.6086 11.9697 22.9024 10.9177 22.925C10.4141 22.9363 10 22.9815 10 23.0154C10 23.1284 11.3654 23.7611 12.16 24.0097C14.5438 24.7441 17.3753 24.4277 19.5017 23.1736C21.0126 22.281 22.5235 20.5071 23.2286 18.7897C23.6091 17.8745 23.9896 16.2023 23.9896 15.4001C23.9896 14.8803 24.0232 14.8125 24.6499 14.1911C25.0192 13.8295 25.3662 13.4341 25.4333 13.3211C25.5452 13.1064 25.534 13.1064 24.9633 13.2985C24.012 13.6375 23.8777 13.5923 24.3477 13.0838C24.6947 12.7223 25.1088 12.0669 25.1088 11.8749C25.1088 11.841 24.9409 11.8975 24.7506 11.9992C24.5492 12.1121 24.1015 12.2816 23.7658 12.3833L23.1614 12.5754L22.613 12.2025C22.3108 11.9992 21.8856 11.7732 21.6617 11.7054C21.0909 11.5472 20.218 11.5698 19.7032 11.7506C18.3042 12.259 17.4201 13.5697 17.5208 15.0046Z"--}}
{{--                        fill="white" />--}}
{{--                </svg>--}}
{{--            </div>--}}
{{--            <img class="blog-single-sec-2-img" src="images/Rectangle 12 (1)12.webp" alt="">--}}
{{--        </div>--}}
{{--        <p class="blog-single-sec-2-p-1">Our blog.dolor sit lorem ipsum</p>--}}
{{--        <p class="blog-single-sec-2-p-2">Lorem ipsum dolor sit amet consectetur. Pellentesque nec cursus consectetur nascetur lectus--}}
{{--            tortor. Volutpat cras diam fringilla vel fames augue consectetur laoreet integer. Nunc mus nunc pulvinar--}}
{{--            odio sed non dictum est lectus. Eget sit pellentesque eget aliquet. Venenatis suspendisse leo ipsum porta--}}
{{--            nisi. Maecenas a tellus ornare ac. Non tortor nisl diam lorem dolor. Pellentesque pellentesque quam arcu--}}
{{--            quis sed ornare. Elementum sed faucibus sodales convallis orci lobortis. Congue sodales accumsan nulla--}}
{{--            adipiscing molestie aliquet cursus. Sed dictum suspendisse id a.</p>--}}
{{--        <div class="blog-single-cont-mega-p">--}}
{{--            <div class="blog-single-sec-2-text-cont-pc">--}}
{{--                <div class="blog-single-text-hidden">--}}
{{--                    <p class="blog-single-sec-2-mega-p">Lorem ipsum dolor sit amet</p>--}}
{{--                </div>--}}
{{--                <div class="blog-single-text-hidden">--}}
{{--                    <p class="blog-single-sec-2-mega-p">consectetur. Blandit</p>--}}
{{--                </div>--}}
{{--                <div class="blog-single-text-hidden">--}}
{{--                    <p class="blog-single-sec-2-mega-p">fermentum praesen lorem</p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="blog-single-sec-2-text-cont-mobile">--}}
{{--                <div class="blog-single-text-hidden">--}}
{{--                    <p class="blog-single-sec-2-mega-p-2">Lorem ipsum dolor</p>--}}
{{--                </div>--}}
{{--                <div class="blog-single-text-hidden">--}}
{{--                    <p class="blog-single-sec-2-mega-p-2">sit amet</p>--}}
{{--                </div>--}}
{{--                <div class="blog-single-text-hidden">--}}
{{--                    <p class="blog-single-sec-2-mega-p-2">consectetur.</p>--}}
{{--                </div>--}}
{{--                <div class="blog-single-text-hidden">--}}
{{--                    <p class="blog-single-sec-2-mega-p-2">Blandit fermentum</p>--}}
{{--                </div>--}}
{{--                <div class="blog-single-text-hidden">--}}
{{--                    <p class="blog-single-sec-2-mega-p-2">praesen lorem</p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="blog-single-sec-2-img-panel">--}}
{{--            <img class="blog-single-sec-2-img" src="{{ asset('images/static/Rectangle 1212.webp') }}" alt="">--}}
{{--        </div>--}}
{{--        <p class="blog-single-sec-2-p-3">Our blog.dolor sit lorem ipsum</p>--}}
{{--        <p class="blog-single-sec-2-p-4">Lorem ipsum dolor sit amet consectetur. Pellentesque nec cursus consectetur nascetur lectus--}}
{{--            tortor. Volutpat cras diam fringilla vel fames augue consectetur laoreet integer. Nunc mus nunc pulvinar--}}
{{--            odio sed non dictum est lectus. Eget sit pellentesque eget aliquet. Venenatis suspendisse leo ipsum porta--}}
{{--            nisi. Maecenas a tellus ornare ac. Non tortor nisl diam lorem dolor. Pellentesque pellentesque quam arcu--}}
{{--            quis sed ornare. Elementum sed faucibus sodales convallis orci lobortis. Congue sodales accumsan nulla--}}
{{--            adipiscing molestie aliquet cursus. Sed dictum suspendisse id a.--}}
{{--            <br><br><br>--}}
{{--            Lorem ipsum dolor sit amet consectetur. Pellentesque nec cursus consectetur nascetur lectus tortor. Volutpat--}}
{{--            cras diam fringilla vel fames augue consectetur laoreet integer. Nunc mus nunc pulvinar odio sed non dictum--}}
{{--            est lectus. Eget sit pellentesque eget aliquet. Venenatis suspendisse leo ipsum porta nisi. Maecenas a--}}
{{--            tellus ornare--}}
{{--        </p>--}}
    </section>
    <section class="blog-single-section-3">
        <div class="blog-single-text-hidden">
            <h3 class="blog-single-sec-3-h3">Popular posts</h3>
        </div>
        <div class="blog-single-sec-3-collaj">
            @foreach($popularBlogList as $blogSingle)
                <div class="blog-info">
                    <a class="blog-a" href="{{ route('blog.single', $blogSingle->slug) }}" target="_blank">
                        <img class="blog-sec-1-img" src="{{ $blogSingle->mainImage->urls['medium'] }}" alt="{{ $blogSingle->mainImage->alt }}">
                        <p class="blog-info-p-1">{{ $blogSingle->title }}</p>
                        <p class="blog-info-p-2">{{ $blogSingle->description }}</p>
                    </a>
                </div>
{{--                <div class="blog-single-sec-3-coll-1">--}}
{{--                    <img class="blog-single-sec-3-img" src="{{ $blog->mainImage->urls['medium'] }}" alt="">--}}
{{--                    <div class="blog-single-sec-3-coll-reverse">--}}
{{--                        <p class="blog-single-sec-3-p-1">Solar pannel</p>--}}
{{--                        <p class="blog-single-sec-3-p-2">Lorem ipsum dolor sit amet consectetur. Blandit fermentum praesen</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
            @endforeach
{{--            --}}
{{--            <div class="blog-single-sec-3-coll-2">--}}
{{--                <img class="blog-single-sec-3-img" src="{{ asset('images/static/Rectangle 12(3).webp') }}" alt="">--}}
{{--                <div class="blog-single-sec-3-coll-reverse">--}}
{{--                    <p class="blog-single-sec-3-p-1">Solar pannel</p>--}}
{{--                    <p class="blog-single-sec-3-p-2">Lorem ipsum dolor sit amet consectetur. Blandit fermentum praesen</p>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
        <div class="blog-single-sec-3-yellow">
            <p class="blog-single-text-yellow-p">Our blog.dolor sit lorem ipsum</p>
            <p class="blog-single-text-yellow-p-2">Lorem ipsum dolor sit amet consectetur. Blandit fermentum praesen</p>
            <div class="blog-single-inp-btn-cont">
                <div class="blog-single-inp-cont">
                    <input class="blog-single-inp-email" type="email" name="" id="" placeholder="your email address">
                </div>
                <button class="blog-single-sec-3-btn">Submit</button>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{ asset('js/gsap-3.11.4-ScrollTrigger.js') }}"></script>
    <script src="{{ asset('js/gsap-3.11.4.js') }}"></script>
    <script src="{{ asset('js/front/blogSingle.js') }}"></script>
@endsection
