@extends('layouts.front')

@section('content')
    <section class="get-in-touch-section-1">
        <div class="get-in-touch-text-map">
            <div class="get-in-touch-text-hidden">
                <h3 class="get-in-touch-text-h3">Get in touch</h3>
            </div>
            <p class="get-in-touch-text-p">Lorem ipsum dolor sit amet consectetur. Convallis nisl aliquet nulla nisi sagittis lacus
                habitasse. Lectus habitasse tristique maecenas id ultrices mi. Lacus est enim consectetur quam mauris
                cras diam.</p>
                <p>alksfdjkjsdbfsdg</p>
            <div class="get-in-touch-map">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3686.010222873076!2d44.52446307623414!3d40.20444501179148!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e1!3m2!1sru!2sam!4v1678955513369!5m2!1sru!2sam"
                    width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                    referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </div>
        <div class="get-in-touch-info-cont">
            <p class="get-in-touch-info-p">Apply for this position:</p>
            <div class="get-in-touch-inp-cont">
                <input class="get-in-touch-inp-text" type="text" name="" id="" placeholder="Full name">
            </div>
            <div class="get-in-touch-inp-cont">
                <input class="get-in-touch-inp-text" type="email" name="" id="" placeholder="Email address">
            </div>
            <div class="get-in-touch-inp-cont">
                <input class="get-in-touch-inp-text" type="text" name="" id="" placeholder="City">
            </div>
            <div class="get-in-touch-inp-cont">
                <input class="get-in-touch-inp-text" type="tel" name="" id="" placeholder="Phone Number">
            </div>
            <div class="get-in-touch-inp-cont get-in-touch-inp-top">
                <input class="get-in-touch-inp-text" type="text" name="" id="" placeholder="Message">
            </div>
            <button class="get-in-touch-btn">Submit
                <svg class="get-in-touch-btn-svg" width="25" height="25" viewBox="0 0 25 25" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path id="btn-svg" d="M7.5 7.5L17.5 17.5" stroke="white" stroke-width="2" stroke-linecap="round"
                          stroke-linejoin="round" />
                    <path id="btn-svg" d="M17.5 7.5V17.5H7.5" stroke="white" stroke-width="2" stroke-linecap="round"
                          stroke-linejoin="round" />
                </svg>
            </button>
        </div>
        <div class="get-in-touch-map-tablet">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3686.010222873076!2d44.52446307623414!3d40.20444501179148!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e1!3m2!1sru!2sam!4v1678955513369!5m2!1sru!2sam"
                width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{ asset('js/gsap-3.11.4-ScrollTrigger.js') }}"></script>
    <script src="{{ asset('js/gsap-3.11.4.js') }}"></script>
    <script src="{{ asset('js/front/getInTouch.js') }}"></script>
@endsection
