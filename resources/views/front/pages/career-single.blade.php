@extends('layouts.front')

@section('content')
    <section class="career-section-1">
        <button class="career-back-btn">
            <a class="career-back-a" href="About.html#designer">
                <div class="career-el-btn">
                    <svg width="43" height="44" viewBox="0 0 43 44" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect id="fill-svg" x="0.149902" y="0.649994" width="42.6" height="42.6" rx="21.3"
                              fill="#F7F7F7" />
                        <path id="stroke-svg" d="M24.0098 15.789L17.8221 21.95L24.0098 28.111" stroke="black"
                              stroke-width="1.99687" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                    <p class="career-btn-text">Back</p>
                </div>
            </a>
        </button>
        <div class="career-text-hidden">
            <h3 class="career-sec-1-h3">PRODUCT DESIGNER</h3>
        </div>
        <div class="career-sec-1-texts-cont">
            <div class="career-group">
                <p class="career-text-cont-p">Who Are We Looking For</p>
                <div class="career-svg-text-cont">
                    <div class="career-horizontal">
                        <svg class="sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="career-horizontal">
                        <svg class="sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="career-horizontal">
                        <svg class="career-sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="career-horizontal">
                        <svg class="career-sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="career-horizontal">
                        <svg class="career-sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="career-horizontal">
                        <svg class="career-sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="career-horizontal">
                        <svg class="career-sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="career-horizontal">
                        <svg class="career-sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="career-horizontal">
                        <svg class="career-sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                </div>
            </div>
            <div class="career-group">
                <p class="career-text-cont-p">Lorem ipsum dolor sit amet</p>
                <div class="career-svg-text-cont">
                    <div class="career-horizontal">
                        <svg class="career-sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="career-horizontal">
                        <svg class="career-sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="career-horizontal">
                        <svg class="career-sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="career-horizontal">
                        <svg class="career-sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="career-horizontal">
                        <svg class="career-sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="career-horizontal">
                        <svg class="career-sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="career-horizontal">
                        <svg class="career-sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="career-horizontal">
                        <svg class="career-sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="career-horizontal">
                        <svg class="career-sec-1-svg" width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.5" width="19" height="19" fill="#18191E" />
                        </svg>
                        <p class="career-svg-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="career-info-cont">
            <div class="career-text-hidden">
                <p class="career-info-p">Apply for this position:</p>
            </div>
            <div class="career-inputs-cont">
                <div class="career-inp-cont">
                    <input class="career-inp-text" type="text" name="" id="" placeholder="Full name">
                </div>
                <div class="career-inp-cont">
                    <input class="career-inp-text" type="email" name="" id="" placeholder="Email address">
                </div>
                <div class="career-inp-cont">
                    <input class="career-inp-text" type="tel" name="" id="" placeholder="Phone Number">
                </div>
                <div class="career-inp-cont">
                    <input class="career-inp-text" type="tel" name="" id="" placeholder="Cover letter">
                </div>
                <div class="career-inp-file">
                    <input class="career-file" type="file" name="" id="">
                    <div class="career-file-new">
                        <svg width="34" height="34" viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M30.3731 15.6541L17.3539 28.6733C15.759 30.2682 13.5958 31.1643 11.3402 31.1643C9.08458 31.1643 6.92137 30.2682 5.32643 28.6733C3.73148 27.0783 2.83545 24.9151 2.83545 22.6595C2.83545 20.4039 3.73148 18.2407 5.32643 16.6458L18.3456 3.62662C19.4089 2.56333 20.851 1.96597 22.3548 1.96597C23.8585 1.96597 25.3006 2.56333 26.3639 3.62662C27.4272 4.68992 28.0246 6.13206 28.0246 7.63579C28.0246 9.13952 27.4272 10.5817 26.3639 11.645L13.3306 24.6641C12.7989 25.1958 12.0779 25.4945 11.326 25.4945C10.5741 25.4945 9.85307 25.1958 9.32143 24.6641C8.78978 24.1325 8.4911 23.4114 8.4911 22.6595C8.4911 21.9077 8.78978 21.1866 9.32143 20.655L21.3489 8.64162"
                                stroke="black" stroke-width="2.83333" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                        <p class="career-file-text">Attach CV</p>
                    </div>
                </div>
            </div>
            <button class="career-btn">Submit
                <svg class="career-btn-svg" width="25" height="25" viewBox="0 0 25 25" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path id="btn-svg" d="M7.5 7.5L17.5 17.5" stroke="white" stroke-width="2" stroke-linecap="round"
                          stroke-linejoin="round" />
                    <path id="btn-svg" d="M17.5 7.5V17.5H7.5" stroke="white" stroke-width="2" stroke-linecap="round"
                          stroke-linejoin="round" />
                </svg>
            </button>
        </div>
        <div class="career-sec-6-info">
            <div class="career-text-hidden">
                <p class="career-info-text">Related positions</p>
            </div>
            <div class="career-info-group">
                <div class="career-info-1">
                    <div class="career-sec-6-text-1-cont">
                        <p class="career-sec-6-text-1">Design</p>
                    </div>
                    <p class="career-sec-6-text-2">UI Designer</p>
                    <div class="career-text-circle-cont">
                        <p class="career-sec-6-text-3">Remote</p>
                        <svg class="career-svg-circle" width="6" height="5" viewBox="0 0 6 5" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M2.94672 4.50258C4.19129 4.50258 5.19072 3.55972 5.19072 2.29629C5.19072 1.01401 4.21015 0.0900062 2.94672 0.0900062C1.68329 0.0900062 0.721575 1.03286 0.721575 2.29629C0.721575 3.54086 1.70215 4.50258 2.94672 4.50258Z"
                                fill="#333333" />
                        </svg>
                        <p class="career-sec-6-text-3">Full time</p>
                    </div>
                    <a class="career-sec-6-click" href="CareerSinglePage.html">
                        <p class="career-click-text">Apply now</p>
                        <svg class="career-sec-6-icon" width="24" height="25" viewBox="0 0 24 25" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M7 7.85693L17 17.8569" stroke="black" stroke-width="2" stroke-linecap="round"
                                  stroke-linejoin="round" />
                            <path d="M17 7.85693V17.8569H7" stroke="black" stroke-width="2" stroke-linecap="round"
                                  stroke-linejoin="round" />
                        </svg>
                    </a>
                </div>
                <div class="career-info-1">
                    <div class="career-sec-6-text-1-cont">
                        <p class="career-sec-6-text-1">Design</p>
                    </div>
                    <p class="career-sec-6-text-2">UI Designer</p>
                    <div class="career-text-circle-cont">
                        <p class="career-sec-6-text-3">Remote</p>
                        <svg class="career-svg-circle" width="6" height="5" viewBox="0 0 6 5" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M2.94672 4.50258C4.19129 4.50258 5.19072 3.55972 5.19072 2.29629C5.19072 1.01401 4.21015 0.0900062 2.94672 0.0900062C1.68329 0.0900062 0.721575 1.03286 0.721575 2.29629C0.721575 3.54086 1.70215 4.50258 2.94672 4.50258Z"
                                fill="#333333" />
                        </svg>
                        <p class="scareer-ec-6-text-3">Full time</p>
                    </div>
                    <a class="career-sec-6-click" href="CareerSinglePage.html">
                        <p class="career-click-text">Apply now</p>
                        <svg class="sec-6-icon" width="24" height="25" viewBox="0 0 24 25" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M7 7.85693L17 17.8569" stroke="black" stroke-width="2" stroke-linecap="round"
                                  stroke-linejoin="round" />
                            <path d="M17 7.85693V17.8569H7" stroke="black" stroke-width="2" stroke-linecap="round"
                                  stroke-linejoin="round" />
                        </svg>
                    </a>
                </div>
            </div>
            <div class="career-info-group career-info-group-top">
                <div class="career-info-1">
                    <div class="career-sec-6-text-1-cont">
                        <p class="career-sec-6-text-1">Design</p>
                    </div>
                    <p class="career-sec-6-text-2">UI Designer</p>
                    <div class="career-text-circle-cont">
                        <p class="career-sec-6-text-3">Remote</p>
                        <svg class="career-svg-circle" width="6" height="5" viewBox="0 0 6 5" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M2.94672 4.50258C4.19129 4.50258 5.19072 3.55972 5.19072 2.29629C5.19072 1.01401 4.21015 0.0900062 2.94672 0.0900062C1.68329 0.0900062 0.721575 1.03286 0.721575 2.29629C0.721575 3.54086 1.70215 4.50258 2.94672 4.50258Z"
                                fill="#333333" />
                        </svg>
                        <p class="career-sec-6-text-3">Full time</p>
                    </div>
                    <a class="career-sec-6-click" href="CareerSinglePage.html">
                        <p class="career-click-text">Apply now</p>
                        <svg class="career-sec-6-icon" width="24" height="25" viewBox="0 0 24 25" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M7 7.85693L17 17.8569" stroke="black" stroke-width="2" stroke-linecap="round"
                                  stroke-linejoin="round" />
                            <path d="M17 7.85693V17.8569H7" stroke="black" stroke-width="2" stroke-linecap="round"
                                  stroke-linejoin="round" />
                        </svg>
                    </a>
                </div>
                <div class="career-info-1">
                    <div class="career-sec-6-text-1-cont">
                        <p class="career-sec-6-text-1">Design</p>
                    </div>
                    <p class="career-sec-6-text-2" p>UI Designer</p>
                    <div class="career-text-circle-cont">
                        <p class="career-sec-6-text-3">Remote</p>
                        <svg class="career-svg-circle" width="6" height="5" viewBox="0 0 6 5" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M2.94672 4.50258C4.19129 4.50258 5.19072 3.55972 5.19072 2.29629C5.19072 1.01401 4.21015 0.0900062 2.94672 0.0900062C1.68329 0.0900062 0.721575 1.03286 0.721575 2.29629C0.721575 3.54086 1.70215 4.50258 2.94672 4.50258Z"
                                fill="#333333" />
                        </svg>
                        <p class="career-sec-6-text-3">Full time</p>
                    </div>
                    <a class="career-sec-6-click" href="CareerSinglePage.html">
                        <p class="career-click-text">Apply now</p>
                        <svg class="sec-6-icon" width="24" height="25" viewBox="0 0 24 25" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path d="M7 7.85693L17 17.8569" stroke="black" stroke-width="2" stroke-linecap="round"
                                  stroke-linejoin="round" />
                            <path d="M17 7.85693V17.8569H7" stroke="black" stroke-width="2" stroke-linecap="round"
                                  stroke-linejoin="round" />
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{ asset('js/gsap-3.11.4-ScrollTrigger.js') }}"></script>
    <script src="{{ asset('js/gsap-3.11.4.js') }}"></script>
    <script src="{{ asset('js/front/service.js') }}"></script>
@endsection
