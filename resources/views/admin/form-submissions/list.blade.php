<script>
    $(document).ready(function () {
        new Table({
            name: "form-submissions",
            // filters: [
            //     {
            //         name: 'type',
            //         options: {
            //             'global': 'Home',
            //             'get_in_touch': 'Get-in-touch',
            //             'career': 'Career',
            //         }
            //     }
            // ],
            columns: [
                {
                    title: 'ID'
                },
                {
                    title: 'Full name',
                    data: 'full_name',
                    render: function (data, type, row) {
                        return `<a class="row-title" href="javascript:">${data}</a>
                                    <div>
                                        <ul class="actions-list">
                                            <li><a  href="/admin/form-submissions/${row.id}/edit">Edit</i></a></li>
                                            <li><button class="delete-confirm-btn" data-popup="delete-confirm-${row.id}">Trash</button></li>
                                        </ul>
                                        <div class="text-center delete-popup delete-confirm-${row.id} d-none">
                                            <div class="mb-1"> Are you sure?</div>
                                            <div class="d-flex justify-content-center">
                                                <form action="/admin/form-submissions/${row.id}" class="delete-form" method="POST">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                        <input type="hidden" name="_token" value="${$('meta[name="csrf-token"]').attr('content')}">
                                                            <button type="submit" class="mr-1 delete_form">
                                                                Yes
                                                            </button>
                                                </form>
                                                <button class="mr-1 delete-popup-close" type="button">
                                                    No
                                                </button>
                                            </div>
                                        </div>
                                    </div>`;
                    }
                },
                {
                    title: 'Type'
                },
                {
                    title: 'Email'
                },
                {
                    title:'Date',
                    type:'date',
                    data: 'created_at'
                },
            ]
        })
    });
</script>
