<!-- left column -->
<div class="col-md-9 main-content--large">
    <div class="row">
        <div class="col-12 card card-default main-card">
            <div class="card-body">
                @include('admin.model.inputs.text', ['entity' => $entity ?? null, 'field' => 'title', 'classes' => 'generate-slug'])

                @include('admin.model.inputs.text', ['entity' => $entity ?? null, 'field' => 'description'])

                @include('admin.model.inputs.text', ['entity' => $entity ?? null, 'field' => 'slug', 'classes' => 'input-slug'])

                <media-uploader prop-input-name="tinymce"></media-uploader>
                @include('admin.model.inputs.textarea', ['entity' => $entity ?? null, 'field' => 'content', 'classes' => 'generate-slug'])

                @include('admin.model.seo-fields', ['entity' => $entity ?? null])

            </div>
        </div>
    </div>
</div>

<!-- right column -->
<div class="col-md-3 main-content--small">
    <div class="row">
        <div class="col-12">
            @include('admin.model.actions-section', ['entity' => $entity ?? null])

            @include('admin.model.inputs.upload-from-media', ['entity' => $entity ?? null, 'field' => 'main_image'])

            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-default main-card">
                                <div data-card-widget="collapse">
                                    <div class="card-header">
                                        <span>Categories</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 category-search-box">
                                            <category-select
                                                :prop-selected="{{json_encode(array_map('intval', array_values(old('categories', isset($entity) ? $entity->categories()->pluck('id')->toArray() : []))))}}"
                                                prop-field="relations[categories]"
                                                :prop-multiple="true"
                                                class="@error('categories') is-invalid @enderror"></category-select>
                                            @error('categories')
                                            <span class="invalid-feedback">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
