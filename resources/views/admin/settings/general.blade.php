@extends('layouts.admin')

@section('title', 'General Settings')

@section('content')
    <div class="content-wrapper admin-pages-edit-content">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-4">
                    <div class="col-md-6">
                        <h1>General settings</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="content container-fluid">
            <form id="create-form" action="{{route('settings.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-9 main-content--large">
                        <div class="row">
                            <div class="col-12 card card-default main-card">
                                <div class="card-body">
                                    <div class="row mb-3">
                                        <div class="col-12">
                                            <label for="phone_number">Phone Number</label>
                                            <div class="input-group-sm">
                                                <input class="form-control" id="phone_number" type="number" name="phone_number" value="{{$settings->get('phone_number') }}">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <label for="phone_number_format">Phone Number With Format</label>
                                            <div class="input-group-sm">
                                                <input class="form-control" id="phone_number_format" type="text" name="phone_number_format" value="{{$settings->get('phone_number_format') }}">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <label for="email">E-mail</label>
                                            <div class="input-group-sm">
                                                <input class="form-control" id="email" type="email" name="email" value="{{$settings->get('email') }}">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <label for="latitude">Latitude</label>
                                            <div class="input-group-sm">
                                                <input class="form-control" id="latitude" type="text" name="latitude" value="{{$settings->get('latitude') }}">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <label for="longitude">Longitude</label>
                                            <div class="input-group-sm">
                                                <input class="form-control" id="longitude" type="text" name="longitude" value="{{$settings->get('longitude') }}">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <label for="google_map_key">Google map Key</label>
                                            <div class="input-group-sm">
                                                <input class="form-control" id="google_map_key" type="text" name="google_map_key" value="{{$settings->get('google_map_key') }}">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <label for="location">Location</label>
                                            <div class="input-group-sm">
                                                <input class="form-control" id="location" type="text" name="location" value="{{$settings->get('location') }}">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <label for="chart_per_solar">Per-Solar Chart Prices</label>
                                            <div class="input-group-sm">
                                                <input class="form-control chart-inputs" id="chart_per_solar" type="text" name="chart_per_solar" value="{{ implode(',', $settings->get('chart_per_solar')) }}">
                                                <span></span>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <label for="chart_post_solar">Post-Solar Chart Prices</label>
                                            <div class="input-group-sm">
                                                <input class="form-control chart-inputs" id="chart_post_solar" type="text" name="chart_post_solar" value="{{implode(',', $settings->get('chart_post_solar')) }}">
                                                <span></span>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <label for="chart_years">Chart Years</label>
                                            <div class="input-group-sm">
                                                <input class="form-control chart-inputs" id="chart_years" type="text" name="chart_years" value="{{implode(',', $settings->get('chart_years')) }}">
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 main-content--small">
                        <div class="row">
                            <div class="col-12">
                                <div class="card card-default main-card">
                                    <div class="card-header" data-card-widget="collapse">
                                        <span>Actions</span>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-4 text-right">
                                                <button class="btn btn-sm btn-primary" form="create-form" type="submit">Update</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('.chart-inputs').on('change', function () {
                const values = $(this).val().split(',').filter(i => {
                    return i && !isNaN(parseInt(i))
                })
                console.log(values);
                if (values.length < 25) {
                    $(this).next('span').css('color', 'red')
                    $(this).next('span').text(`Count of input value most be 25 Current: ${values.length}`)
                } else if (values.length === 25){
                    $(this).next('span').css('color', '')
                    $(this).next('span').text(``)
                }
            })
        })
    </script>
@endsection
