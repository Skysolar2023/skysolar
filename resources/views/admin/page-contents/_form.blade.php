<!-- left column -->
<div class="col-md-9 main-content--large">
    <div class="row">
        <div class="col-12 card card-default main-card">
            <div class="card-body">
                @include('admin.model.inputs.text', ['entity' => $entity ?? null, 'field' => 'title', 'classes' => 'generate-title'])

                @include('admin.model.inputs.text', ['entity' => $entity ?? null, 'field' => 'subtitle', 'classes' => 'generate-slug'])

                @include('admin.model.inputs.text', ['entity' => $entity ?? null, 'field' => 'section_id', 'classes' => 'generate-slug'])

                @include('admin.model.inputs.select', ['entity' => $entity ?? null, 'field' => 'type', 'classes' => 'generate-slug',
                    'options' => [
                        [
                            'option' => 'Select',
                            'value' => null,
                        ],
                        [
                            'option' => \App\Models\PageContent::TYPE_HOMEPAGE,
                            'value' => \App\Models\PageContent::TYPE_HOMEPAGE,
                        ]
                    ]
                ])

                @include('admin.model.inputs.select', ['entity' => $entity ?? null, 'field' => 'content_type', 'classes' => 'generate-slug',
                    'options' => [
                        [
                            'option' => \App\Models\PageContent::CONTENT_TYPE_HTML,
                            'value' => \App\Models\PageContent::CONTENT_TYPE_HTML,
                        ],
                        [
                            'option' => \App\Models\PageContent::CONTENT_TYPE_VIDEO_URL,
                            'value' => \App\Models\PageContent::CONTENT_TYPE_VIDEO_URL,
                        ]
                    ]
                ])

                <div id="content_html_wrap" class="content-field-wrapper {{  @$entity->content_type !== \App\Models\PageContent::CONTENT_TYPE_HTML ? 'd-none' : '' }}">
                    @include('admin.model.inputs.textarea', ['entity' => $entity ?? null, 'field' => 'content', 'tinymce' => false])
                </div>
                <div id="content_video_wrap" class="content-field-wrapper {{ @$entity->content_type !== \App\Models\PageContent::CONTENT_TYPE_VIDEO_URL ? 'd-none' : '' }}">
                    @php
                    if (isset($entity)) {
                        $entity->video_url = $entity->content;
                    }
                    @endphp
                    @include('admin.model.inputs.text', ['entity' => $entity ?? null, 'field' => 'video_url'])
                </div>
            </div>
        </div>
    </div>
</div>

<!-- right column -->
<div class="col-md-3 main-content--small">
    <div class="row">
        <div class="col-12">
            @include('admin.model.actions-section', ['entity' => $entity ?? null])

            @include('admin.model.inputs.upload-from-media', ['entity' => $entity ?? null, 'field' => 'image'])
        </div>
    </div>
</div>
@section('scripts')
    <script>
        $(document).ready(function () {


            console.log("$([name=content]:visible)", $('[name="content"]:visible'))
            if(!$('[name="content"]:visible').length) {
                showCorrectContent($('[name="content_type"]').val())
            }

            $('[name="content_type"]').change(function () {
                showCorrectContent($(this).val())
            })

            function showCorrectContent(value) {
                $('.content-field-wrapper').addClass('d-none')
                switch (value) {
                    case '{{ \App\Models\PageContent::CONTENT_TYPE_HTML }}' :
                        $('#content_html_wrap').removeClass('d-none');
                        break;
                    case '{{ \App\Models\PageContent::CONTENT_TYPE_VIDEO_URL }}' :
                        $('#content_video_wrap').removeClass('d-none');
                        break;
                }
            }
        })
    </script>
@endsection
