@extends('layouts.front')

@section('content')
    <div class="content-404-page">
        <div class="container tn_container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="ttn_content content-box">
                        <h1>404</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
