<div marginwidth="0" marginheight="0" style="padding:0">
    <div style="display: flex; align-items: center; justify-content: center;flex-direction: column">
        <h2>Thank you {{ $data['full_name'] }}</h2>
        <br>
        <p>We'll be in touch.</p>
    </div>
</div>
