export default (data) => {
    let formatter = new Intl.NumberFormat('am-AM', {maximumFractionDigits: 0});
    return `${formatter.format(data)} ֏`
}
