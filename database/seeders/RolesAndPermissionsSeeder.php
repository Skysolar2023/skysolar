<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        Role::updateOrCreate(['name' => 'admin'], ['name' => 'admin']);
        Role::updateOrCreate(['name' => 'Super Admin'], ['name' => 'Super Admin']);

        // create permissions
        $permissions = [
            'Manage Pages',
        ];

        foreach ($permissions as $permission) {
            Permission::updateOrCreate(['name' => $permission], ['name' => $permission]);
        }

        //creating Super Admin
        $superAdmin = User::updateOrCreate([
            'email' => config('app.super_admin.email'),
        ], [
            'first_name' => config('app.super_admin.name'),
            'status' => 'active',
            'last_name' => config('app.super_admin.name'),
            'email' => config('app.super_admin.email'),
            'password' => Hash::make(config('app.super_admin.password')),
        ]);
        $superAdmin->assignRole('Super Admin');
    }
}
