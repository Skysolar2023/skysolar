<?php

namespace Database\Seeders;

use App\Models\Setting;
use App\Services\SettingsService;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::truncate();

        $seeder = SettingsService::DEFAULT_SETTINGS;
        foreach ($seeder as $key => $setting) {
            Setting::create([
                'key' => $key,
                'value' => is_array($setting) ? json_encode($setting) : $setting
            ]);
        }

    }
}
