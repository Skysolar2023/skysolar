<?php
namespace App\Services;

use App\Models\PageContent;

class PagesService {

    private $sections;

    public function __construct()
    {
        $sections = PageContent::all()->load('mainImage');
        $this->sections = [];
        foreach ($sections as $section) {
            $this->sections[$section->type][$section->section_id][] = $section;
        }
    }

    public function get($page, $section, $expect = null)
    {
        if (isset($this->sections[$page][$section])) {
            return $this->normalize($this->sections[$page][$section], $expect);
        }

        return $this->normalize([], $expect);
    }

    public function first($page, $section, $expect = null)
    {
        if (isset($this->sections[$page][$section]) && count($this->sections[$page][$section])) {
            return $this->normalize($this->sections[$page][$section][0], $expect);
        }

        return $this->normalize(null, $expect);
    }

    private function normalize($value, $expect) {
        switch ($expect) {
            case 'array': {
                return is_array($value) ? $value : [];
            }
            case 'bool': {
                return is_bool($value) ? $value : false;
            }
            default: {
                return $value;
            }
        }
    }
}
