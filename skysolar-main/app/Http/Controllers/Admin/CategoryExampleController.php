<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;

class CategoryExampleController extends ResourceController
{
    protected $resource = 'categories-example';
    protected $modelName = 'CategoryExample';

    public function list()
    {
        return response()->json(['categories' => $this->model::all()]);
    }
}
