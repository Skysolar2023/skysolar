<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\FormSubmissionsRequest;
use App\Mail\ContactMail;
use App\Models\Attachment;
use App\Models\FormSubmission;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Client;

class FormSubmissionController extends Controller
{
    /**
     * @param FormSubmissionsRequest $request
     * @return JsonResponse|RedirectResponse
     * @throws GuzzleException
     */
    public function form(FormSubmissionsRequest $request)
    {

        $data = $request->all();
        $subject = $data['subject'] ?? '';
        unset($data['_token']);
        unset($data['subject']);

        if (isset($data['attachment'])) {
            $uploaded = Attachment::upload($request->file('attachment'));
            unset($data['attachment']);
            $data['attachment'] = $uploaded['path'] . '.' . $uploaded['format'];
        }

        FormSubmission::query()->create($data);
        $data['subject'] = $subject;

        $this->sendEmail($data);

//        $this->mondayApi($data);

        //Todo add career relation

        if ($request->ajax()) {
            return response()->json(['success' => true]);
        }

        return back()->with('success', 'Form sent');
    }

    /**
     * @param $data
     * @return void
     */
    public function sendEmail($data)
    {
        try {
            Mail::to([env('MAIL_TO')])->send(new ContactMail($data));
        } catch (\Exception $e) {
            report($e->getMessage());
        }
    }

    /**
     * @param $sendData
     * @return void
     * @throws GuzzleException
     */
    public function mondayApi($sendData)
    {
        $client = new Client([
            'base_uri' => 'https://api.monday.com/v2/',
            'headers' => [
                'Authorization' => env('MONDAY_API_KEY'),
                'Content-Type' => 'application/json',
                'Accept' => 'application/json'
            ]
        ]);

        $data['column_values'] = $sendData;
        $data['board_id'] = '4171878705';

        $response = $client->post('boards/{board_id}/items', [
            'json' => $data,
            'path' => [
                'board_id' => '4171878705'
            ]
        ]);

        $res = json_decode($response->getBody(), true);

        dd($res);
    }
}
