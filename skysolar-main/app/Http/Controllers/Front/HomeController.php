<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Mail\ContactMail;
use App\Mail\BookingMail;
use App\Models\Blog;
use App\Models\Car;
use App\Models\Category;
use App\Models\CategoryExample;
use App\Models\Customer;
use App\Models\Example;
use App\Models\Faq;
use App\Services\SettingsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    private $settings;
    public function __construct(SettingsService $settings)
    {
        $this->settings = $settings;
    }

    public function index()
    {
        return view('front.home');
    }

    public function about()
    {
        return view('front.pages.about');
    }

    public function services()
    {
        return view('front.pages.services');
    }

    public function examples()
    {
        $categories = CategoryExample::active()->get();

        $examplesQuery = Example::active();

        if (request()->category && request()->category !== 'all'){
            $examplesQuery->whereHas('categories', function ($q) {
                return $q->where('slug', request()->category);
            });
        }
        $examples = $examplesQuery->orderBy('id', 'desc')->paginate(4);

        return view('front.pages.examples', compact('categories', 'examples'));
    }

    public function exampleSingle($slug)
    {
        $example = Example::with('attachments')->active()->where('slug', $slug)->firstOrFail();

        return view('front.pages.examples-single-page', compact('example'));
    }

    public function roof()
    {
        return view('front.pages.roof');
    }

    public function blog()
    {
        $categories = Category::active()->get();

        $blogListQuery = Blog::active();
        if (request()->category && request()->category !== 'all'){
            $blogListQuery->whereHas('categories', function ($q) {
                return $q->where('slug', request()->category);
            });
        }
        $blogList = $blogListQuery->orderBy('id', 'desc')->paginate(4);

        return view('front.pages.blog', compact('categories', 'blogList'));
    }

    public function blogSingle($slug)
    {
        $blog = Blog::active()->where('slug', $slug)->firstOrFail();
        $popularBlogList = Blog::active()->orderBy('id', 'desc')->where('slug', '<>', $slug)->paginate(2);
        return view('front.pages.blog-single', compact('blog', 'popularBlogList'));
    }

    public function getInTouch()
    {
        return view('front.pages.get-in-touch');
    }

    public function careerSingle($slug)
    {
        return view('front.pages.career-single');
    }
}
