<?php

namespace App\Models;

class Customer extends AppModel
{
    const rules = [
        'status' => 'required|string',
        'name' => 'required|string|max:255',
        'content' => 'string|nullable',
        'location' => 'string|nullable',
        'main_image' => 'required|numeric',
    ];

    protected $searchFields = ['id', 'name', 'location'];

    //    RELATIONS
    public function mainImage(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Attachment::class, 'id', 'main_image');
    }
}
