<?php

namespace App\Models;

class Menu extends AppModel
{
    public const rules = [
        'type' => 'required|string|max:255',
        'title' => 'required|string|max:255',
        'active' => 'nullable|numeric',
        'items' => 'nullable',
    ];

    public $timestamps = false;

    public function getItemsAttribute($value)
    {
        return json_decode($value) ?? [];
    }

}
