<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormSubmission extends AppModel
{
    use HasFactory;

    const TYPE_GLOBAL = 'global';
    const TYPE_GET_IN_TOUCH = 'get_in_touch';
    const TYPE_CAREER = 'career';

    protected $guarded = [];
}
