<?php

namespace App\Models;

class Faq extends AppModel
{
    const rules = [
        'status' => 'required|string',
        'title' => 'required|string|max:255',
        'content' => 'string|nullable',
    ];

    protected $searchFields = ['id', 'title', 'content'];
}
