<?php

use Illuminate\Support\Facades\Route;

Route::get('/', [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index');
Route::get('/about', [App\Http\Controllers\Front\HomeController::class, 'about'])->name('about');
Route::get('/services', [App\Http\Controllers\Front\HomeController::class, 'services'])->name('services');
Route::get('/examples', [App\Http\Controllers\Front\HomeController::class, 'examples'])->name('examples');
Route::get('/examples/{slug}', [App\Http\Controllers\Front\HomeController::class, 'exampleSingle'])->name('examples-single');
Route::get('/roof', [App\Http\Controllers\Front\HomeController::class, 'roof'])->name('roof');
Route::get('/blog', [App\Http\Controllers\Front\HomeController::class, 'blog'])->name('blog');
Route::get('/blog/{slug}', [App\Http\Controllers\Front\HomeController::class, 'blogSingle'])->name('blog.single');
Route::get('/get-in-touch', [App\Http\Controllers\Front\HomeController::class, 'getInTouch'])->name('getInTouch');
Route::get('/career/{slug}', [App\Http\Controllers\Front\HomeController::class, 'careerSingle'])->name('career-single');

Route::post('/form-submit', [App\Http\Controllers\Front\FormSubmissionController::class, 'form'])->name('front.formSubmit');



