gsap.registerPlugin(ScrollTrigger);

gsap.to(".roof-h1_text", {
    scrollTrigger: ".roof-h1_text ",
    y: 0,
    duration: .8,
})

gsap.to(".roof-sec-1-h3", {
    scrollTrigger: ".roof-sec-1-h3",
    y: 0,
    duration: .8,
})

gsap.to(".roof-sec-1-text-img-cont", {
    scrollTrigger: ".roof-sec-1-text-img-cont",
    y: 0,
    duration: .8,
})
gsap.to(".roof-texts-cont", {
    scrollTrigger: ".roof-texts-cont",
    y: 0,
    duration: .8,
})

gsap.to(".roof-sec-2-h3", {
    scrollTrigger: ".roof-sec-2-h3",
    y: 0,
    duration: .8,
})

gsap.to(".roof-yellow-h4", {
    scrollTrigger: ".roof-yellow-h4",
    y: 0,
    duration: .8,
})

gsap.to(".roof-yellow-h4-2", {
    scrollTrigger: ".roof-yellow-h4-2",
    y: 0,
    duration: .8,
})

gsap.to(".roof-sec-3-h3", {
    scrollTrigger: ".roof-sec-3-h3",
    y: 0,
    duration: .8,
})

gsap.to(".roof-text-h3-p-cont", {
    scrollTrigger: ".roof-text-h3-p-cont",
    y: 0,
    duration: .8,
})

var a = 0;
$(window).scroll(function () {
    var oTop = $("#counter-box").offset().top - window.innerHeight;
    if (a == 0 && $(window).scrollTop() > oTop) {
        $(".roof-counter").each(function () {
            var $this = $(this),
                countTo = $this.attr("data-number");
            $({
                countNum: $this.text()
            }).animate(
                {
                    countNum: countTo
                },
                {
                    duration: 5000,
                    easing: "swing",
                    step: function () {
                        $this.text(
                            Math.ceil(this.countNum).toLocaleString("en")
                        );
                    },
                    complete: function () {
                        $this.text(
                            Math.ceil(this.countNum).toLocaleString("en")
                        );
                    }
                }
            );
        });
        a = 1;
    }
});

gsap.to(".roof-sec-4-h3", {
    scrollTrigger: ".roof-sec-4-h3",
    y: 0,
    duration: .8,
})

gsap.to(".roof-sec-4-h3-2", {
    scrollTrigger: ".roof-sec-4-h3-2",
    y: 0,
    duration: .8,
})


gsap.to(".roof-sec-4-img", {
    scrollTrigger: {
        scrub: 1,
    },
    y: -900,
});
