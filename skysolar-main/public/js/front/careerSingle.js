gsap.registerPlugin(ScrollTrigger);

gsap.to(".career-info-text", {
    scrollTrigger: ".career-info-text",
    y: 0,
    duration: .8,
})

gsap.to(".career-info-p", {
    scrollTrigger: ".career-info-p",
    y: 0,
    duration: .8,
})

gsap.to(".career-sec-1-h3", {
    scrollTrigger: ".career-sec-1-h3",
    y: 0,
    duration: .8,
})
