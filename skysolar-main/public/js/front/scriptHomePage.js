gsap.registerPlugin(ScrollTrigger);

gsap.to(".home_page_h1_text", {
    scrollTrigger: ".home_page_h1_text ",
    y: 0,
    duration: .8,
})

gsap.timeline({
    scrollTrigger: {
        trigger: ".home_page_section_1",
        markers: true,
        pin: true,
        scrub: .5,
    }
})

    .from(".home_page_outsole", { y: '-85vh', stagger: .4 })

gsap.timeline({
    scrollTrigger: {
        trigger: ".home_page_cont_img_2_hand",
        markers: true,
        pin: true,
        scrub: 8,
        start: "bottom center",
        end: "top center",
    }
})
    .to(".home_page_outsole_2", { y: "15vh", x: '-25vh', stagger: 10 })
gsap.timeline({
    scrollTrigger: {
        trigger: ".home_page_cont_img_1_hand",
        markers: true,
        pin: true,
        scrub: 8,
        start: "top center",
        end: "bottom bottom",
    }
})
    .to(".home_page_outsole_1", { y: "-5vh", x: "25vh", stagger: 10 })

gsap.to(".home_page_sec_2_text_h4", {
    scrollTrigger: ".home_page_sec_2_text_h4 ",
    y: 0,
    duration: .8,
})

gsap.to(".home_page_sec_2_img", {
    scrollTrigger: {
        scrub: 1,
    },
    y: -400,
});


gsap.registerPlugin(ScrollTrigger, ScrollSmoother);

let smoother = ScrollSmoother.create({
    smooth: 2
});

const scaleImage = () => {
    const image = document.querySelector(".image-scale__right img");
    const imageWrapper = document.querySelector(".image-scale__right");
    const tl = gsap.timeline({
        scrollTrigger: {
            trigger: ".image-scale__section",
            start: "top top",
            pin: true,
            end: "+=1500",
            scrub: true
        }
    });

    tl.to('.home_page_sec_4_texts_cont', {
        y: -2700,
        duration: 90,
        scrub: .1,
    }, 30)

    tl.to(
        imageWrapper,
        {
            xPercent: 50,
            duration: 20,
        },
        0
    )

        .to(
            image,
            {
                xPercent: -40,
                duration: 20,
            },
            0
        )

        .to(
            image,
            {
                transformOrigin: "bottom center",
                scale: 0.9,
                duration: 70,
            },
            30
        )

        .to(
            ".box--one",
            {
                scaleY: 1,
                duration: 10
            },
            30
        )

        .to(
            ".box--two",
            {
                scaleX: 1,
                duration: 10
            },
            40
        )

        .to(
            ".box--three",
            {
                scaleY: 0.7,
                duration: 10
            },
            50
        )

        .to(
            ".box--four",
            {
                scaleX: 1,
                duration: 10
            },
            60
        );

    return tl;
};

const master = gsap.timeline();

master.add(scaleImage(".image-scale__right"));



gsap.registerPlugin(ScrollTrigger, ScrollSmoother);

let smoother2 = ScrollSmoother.create({
    smooth: 2
});

const scaleImage2 = () => {
    const imageWrapper2 = document.querySelectorAll(".home_page_image_scroll_2");
    const tl2 = gsap.timeline({
        scrollTrigger: {
            trigger: ".home_page_section_5",
            start: "top top",
            pin: true,
            end: "+=1500",
            scrub: true
        }
    });
    for (let i = 0; i < imageWrapper2.length; i++) {
        tl2.to(
            imageWrapper2[i],
            {
                scale: 0.5,
                duration: 3800,
            },
            0
        )
    }

    tl2.to(
        '.home_page_texts', {
            y: - 3800,
            duration: 3800,
        },
        0
    )

    return tl2;
};
const master2 = gsap.timeline();
master2.add(scaleImage2(".home_page_image_scroll_2"));



gsap.registerPlugin(ScrollTrigger, ScrollSmoother);
let smoother3 = ScrollSmoother.create({
    smooth: 2
});

const scaleImage3 = () => {
    const tl3 = gsap.timeline({
        scrollTrigger: {
            trigger: ".home_page_svg_color_blue",
            start: "top 30%",
            pin: true,
            end: "+=6820",
            scrub: true,
            markers: true,
        }
    });

    const tl4 = gsap.timeline({
        scrollTrigger: {
            trigger: ".home_page_left_hand",
            start: "top top",
            pin: true,
            end: "+=6820",
        }
    });

    const tl5 = gsap.timeline({
        scrollTrigger: {
            trigger: ".home_page_right_hand",
            start: "bottom 110%",
            pin: true,
            end: "+=6820",
        }
    });

    gsap.to(".home_page_sec_7_img", {
        scrollTrigger: {
            scrub: 1,
        },
        y: -900,
    });

    gsap.to(".home_page_parallax_img", {
        y: "-10%",
        ease: "power2.out",
        scrollTrigger: {
            trigger: ".home_page_sec_10_img_cont",
            start: "top top",
            end: "bottom bottom",
            scrub: true
        }
    });

    gsap.to(".home_page_sec_7_h4", {
        scrollTrigger: ".home_page_sec_7_h4 ",
        y: 0,
        duration: .8,
    })

    gsap.to(".home_page_sec_8_h4", {
        scrollTrigger: ".home_page_sec_8_h4 ",
        y: 0,
        duration: .8,
    })

    gsap.to(".home_page_sec_h4", {
        scrollTrigger: ".home_page_sec_h4 ",
        y: 0,
        duration: .8,
    })

    gsap.to(".home_page_sec_10_h4", {
        scrollTrigger: ".home_page_sec_10_h4 ",
        y: 0,
        duration: .8,
    })

    gsap.to(".home_page_sec_11_h4", {
        scrollTrigger: ".home_page_sec_11_h4 ",
        y: 0,
        duration: .8,
    })

    gsap.to(".home_page_sec_12_h4", {
        scrollTrigger: ".home_page_sec_12_h4 ",
        y: 0,
        duration: .8,
    })

    gsap.to(".home_page_sec_12_h4_display", {
        scrollTrigger: ".home_page_sec_12_h4_display",
        y: 0,
        duration: .8,
    })

    gsap.to(".home_page_sec_12_h5", {
        scrollTrigger: ".home_page_sec_12_h5",
        y: 0,
        duration: .8,
    })

    gsap.to(".home_page_sec_13_h4", {
        scrollTrigger: ".home_page_sec_13_h4",
        y: 0,
        duration: .8,
    })

    return tl3;
    return tl4;
    return tl5;
};
const master3 = gsap.timeline();
master3.add(scaleImage3(".home_page_image_cont_sec_6"));



let btnItem = document.getElementById('loadMore')
$(document).ready(function(){
    $(".content").slice(0, 2).show();
    $("#loadMore").on("click", function(e){
        e.preventDefault();
        $(".content:hidden").slice(0, 2).slideDown();
        if($(".content:hidden").length == 0) {
            btnItem.style.display = 'none'
        }
    });
})


$(document).ready(function () {
    $('.single-portfolio .video').each(function (index, el) {
        $(this).click(function (event) {
            $(this).siblings('.full').fadeIn(10)
        });
    });
    $('.single-portfolio .cursor').each(function (index, el) {
        $(this).click(function (event) {
            $(this).siblings('.full').fadeIn(10)
        });
    });
});


let full = document.querySelectorAll(".full");
let close = document.querySelectorAll(".close");
let center = document.querySelectorAll(".center");

for (let i = 0; i < close.length; i++) {
    close[i].onclick = () => {
        for (let j = 0; j < full.length; j++) {
            full[j].style.display = "none"
        }
    }
}

window.onkeydown = (event) => {
    if (event.keyCode == 27) {
        for (let j = 0; j < full.length; j++) {
            full[j].style.display = "none"
        }
    }
}

for (let i = 0; i < center.length; i++) {
    center[i].onclick = () => {
        for (let j = 0; j < full.length; j++) {
            full[j].style.display = "none"
        }
    }
}

let swiper = new Swiper('.swiper-container', {
    slidesPerView: 1,
    spaceBetween: 0,
    loop: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});




document.addEventListener('DOMContentLoaded', () => {
    window.onload = function() {
        let image_circle = document.querySelector(".home_page_sec_5_img_cont2");
        let inner = document.querySelectorAll(".home_page_swiper-slide-img-cont2");
        if (inner[1].left == '200%') {
            image_circle.style.marginTop = "-200px";
        }
    };
})


let nextBtn = document.getElementById("home_page_next")
let prevBtn = document.getElementById("home_page_prev")
let swiperSlider = document.querySelector(".home_page_swiper-wrapper")

let num = 0;

nextBtn.onclick = () => {
    num += 100
    if(num > 400) {
        num = 0
    }
    swiperSlider.style.left = -num + '%'
}
prevBtn.onclick = () => {
    num -= 100
    if(num < 0) {
        num = 400
    }
    swiperSlider.style.left = -num + '%'
}
