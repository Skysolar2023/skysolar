gsap.registerPlugin(ScrollTrigger);

gsap.to(".blog-single-sec-1-h3", {
    scrollTrigger: ".blog-single-sec-1-h3",
    y: 0,
    duration: .8,
})

gsap.to(".blog-single-sec-1-h3-color-yellow", {
    scrollTrigger: ".blog-single-sec-1-h3-color-yellow",
    y: 0,
    duration: .8,
})

gsap.to(".blog-single-sec-2-h3", {
    scrollTrigger: ".blog-single-sec-2-h3",
    y: 0,
    duration: .8,
})

gsap.to(".blog-single-sec-2-mega-p", {
    scrollTrigger: ".blog-single-sec-2-mega-p",
    y: 0,
    duration: .8,
})

gsap.to(".blog-single-sec-2-mega-p-2", {
    scrollTrigger: ".blog-single-sec-2-mega-p-2",
    y: 0,
    duration: .8,
})

gsap.to(".blog-single-sec-3-h3", {
    scrollTrigger: ".blog-single-sec-3-h3",
    y: 0,
    duration: .8,
})
