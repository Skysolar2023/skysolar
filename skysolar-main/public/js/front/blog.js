gsap.registerPlugin(ScrollTrigger);

gsap.to(".blog-sec-1-h3", {
    scrollTrigger: ".blog-sec-1-h3",
    y: 0,
    duration: .8,
})

gsap.to(".blog-sec-1-h4", {
    scrollTrigger: ".blog-sec-1-h4",
    y: 0,
    duration: .8,
})

let s6 = new Swiper('.blog-swiper-container', {
    slidesPerView: 'auto',
    slideToClickedSlide: true,
});
const swiperSlides = document.getElementsByClassName('blog-swiper-slide');
s6.on('slideChange', function () {
    const otherSlides = swiperSlides
    for (let index = 0; index < swiperSlides.length; index++) {
        const element = swiperSlides[index];
    }
});

function loadNextSeven() {
    let moveItems = $('#off-items-bucket .blog-item').slice(0, 1);
    moveItems.hide().appendTo('#on-items-bucket').fadeIn('medium');
}
function isThisTheEnd() {
    let numberLeft = $('#off-items-bucket .blog-item').length;
    if (numberLeft == 0) {
        $('#load-more').hide();
    }
}
$(document).ready(function () {
    loadNextSeven();
    isThisTheEnd();
});
$('#load-more').click(function () {
    loadNextSeven();
    isThisTheEnd();
});
