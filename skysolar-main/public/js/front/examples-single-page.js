gsap.registerPlugin(ScrollTrigger);

gsap.to(".sec-1-h3", {
    scrollTrigger: ".sec-1-h3 ",
    y: 0,
    duration: .8,
})

gsap.to(".sec-2-h3", {
    scrollTrigger: ".sec-2-h3 ",
    y: 0,
    duration: .8,
})

gsap.to(".sec-3-h3", {
    scrollTrigger: ".sec-3-h3 ",
    y: 0,
    duration: .8,
})

function loadNextSeven() {
    let moveItems = $('#off-items-bucket .item').slice(0, 1);
    moveItems.hide().appendTo('#on-items-bucket').fadeIn('medium');
}
function isThisTheEnd() {
    let numberLeft = $('#off-items-bucket .item').length;
    if (numberLeft == 0) {
        $('#load-more').hide();
    }
}
$(document).ready(function () {
    loadNextSeven();
    isThisTheEnd();
});
$('#load-more').click(function () {
    loadNextSeven();
    isThisTheEnd();
});


let mobileHeaderMenu = document.querySelector('.mobile-header-menu');
let disNone = document.querySelector('.dis-none');
let mobileMenu = document.querySelector('.mobile-menu');

mobileHeaderMenu.onclick = () => {
    mobileHeaderMenu.style.display = 'none';
    disNone.style.display = 'block';
    mobileMenu.style.display = 'flex';
}

disNone.onclick = () => {
    disNone.style.display = 'none';
    mobileHeaderMenu.style.display = 'block';
    mobileMenu.style.display = 'none';
}
