<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageAttachmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('attachment', function (Blueprint $table) {
//            $table->foreignId('page_id')->references('id')->on('pages')
//                ->onDelete('cascade');
//
//            $table->foreignId('attachment_id')->references('id')->on('attachments')
//                ->onDelete('cascade');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_attachment');
    }
}
