<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <base href="{{url('/')}}">
    <meta name="robots" content="max-image-preview:large">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title', config('app.name', 'Laravel'))</title>

    <!-- Main CSS File -->
    <link rel="stylesheet" href="{{ asset('/dist/front/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/HomePage.css') }}">
    <link rel="stylesheet" href="Responsive/HomePage.css">
    @yield('styles')
</head>
<body>
<div id="front">
    <div class="page-wrapper">

{{--        @include('front.partials.header')--}}

        <main class="main">

            @yield('content')

        </main><!-- End .main -->

{{--        @include('front.partials.footer')--}}
    </div>
    <!-- End .page-wrapper -->

</div>
<script>
    const _configs = JSON.parse('<?=json_encode([
        'baseUrl' => url('/'),
        'email' => config('app.email'),
    ])?>')
</script>


@yield('scripts')
</body>
</html>
