<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <base href="{{url('/')}}">
    <meta name="robots" content="max-image-preview:large">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title', config('app.name', 'Sky Solar'))</title>

    <!-- Favicon -->

{{--    <script type="text/javascript">--}}
{{--        WebFontConfig = {--}}
{{--            google: { families: [ 'Open+Sans:300,400,600,700,800','Poppins:300,400,500,600,700', 'Shadows+Into+Light:400' ] }--}}
{{--        };--}}
{{--        (function(d) {--}}
{{--            var wf = d.createElement('script'), s = d.scripts[0];--}}
{{--            wf.src = '/js/webfont.js';--}}
{{--            wf.async = true;--}}
{{--            s.parentNode.insertBefore(wf, s);--}}
{{--        })(document);--}}
{{--    </script>--}}
    <!-- Plugins CSS File -->


    <!-- Main CSS File -->
    <link rel="stylesheet" href="{{ asset('/dist/front/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('/fontawesome-free/css/all.min.css') }}">
{{--    <link rel="stylesheet" href="{{ asset('css/HomePage.css') }}">--}}
{{--    <link rel="stylesheet" href="{{ asset('css/responsive/HomePage.css') }}">--}}
    @yield('styles')
</head>
<body>
<div id="front">
{{--    <div class="page-wrapper">--}}

        @include('front.partials.header')


            @yield('content')


        @include('front.partials.footer')
{{--    </div>--}}
    <!-- End .page-wrapper -->

</div>
<script>
    const _configs = JSON.parse('<?=json_encode([
        'baseUrl' => url('/'),
        'email' => config('app.email'),
    ])?>')
</script>

<script src="{{ asset('/js/jquery.min.js') }}"></script>
<script src="{{ asset('/js/swiper-4.3.0.js') }}"></script>
<script src="{{ asset('/js/jquery-easing-1.3.js') }}"></script>
<script src="{{ asset('js/gsap-latest-beta.js') }}"></script>
<script src="{{ asset('js/scroll-trigger.js') }}"></script>
<script src="{{ asset('js/scrollsmoother.js') }}"></script>
<script src="{{ asset('js/gsap-3.11.4-ScrollTrigger.js') }}"></script>
<script src="{{ asset('js/gsap-3.11.4.js') }}"></script>
<script src="{{ asset('js/front/header.js') }}"></script>
<script src="{{ asset('js/front/footer.js') }}"></script>
<script src="{{ asset('/dist/front/js/front.js') }}"></script>
@yield('scripts')

{{--<script src="{{ asset('/js/popper.min.js') }}"></script>--}}
{{--<script src="{{ asset('/js/bootstrap.min.js') }}"></script>--}}
{{--<script src="{{ asset('/js/plugins/jquery.magnific-popup.min.js') }}"></script>--}}
{{--<script src="{{ asset('/js/plugins.js') }}"></script>--}}
{{--<script src="{{ asset('/dist/front/js/main.js') }}"></script>--}}
</body>
</html>
