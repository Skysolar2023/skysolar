@extends('layouts.front')

@section('content')
    <section class="home_page_section_1">
        <div class="home_page_cont_img_1_hand">
            <img class="home_page_outsole_1 home_page_hand_image"
                 src="{{ asset('images/static/unsplash_36g19a7ivrs (1).webp') }}" alt="">
        </div>
        <div class="home_page_cont_img_2_hand">
            <img class="home_page_outsole_2 home_page_hand_image"
                 src="{{ asset('images/static/unsplash_36g19a7ivrs.webp') }}" alt="">
        </div>
        <div class="home_page_hand_right">
            <img class="home_page_img_hand" src="{{ asset('images/static/unsplash_36g19a7ivrs (1).webp') }}" alt="">
        </div>
        <div class="home_page_hand_left">
            <img class="home_page_img_hand" src="{{ asset('images/static/unsplash_36g19a7ivrs.webp') }}" alt="">
        </div>
        <div class="home_page_div_image_panel">
            <img class="home_page_arev_img" src="{{ asset('images/static/Frame.webp') }}" alt="">
        </div>
        <div class="home_page_cont_text_img">
            <div class="home_page_h1_cont">
                <div class="home_page_text_hidden">
                    <p class="home_page_h1_text">A Brighter Future</p>
                </div>
                <div class="home_page_text_hidden">
                    <p class="home_page_h1_text">Starts Today.</p>
                </div>
            </div>
            <div class="home_page_h1_cont_mobile">
                <div class="home_page_text_hidden">
                    <p class="home_page_h1_text">A Brighter</p>
                </div>
                <div class="home_page_text_hidden">
                    <p class="home_page_h1_text">Future Starts</p>
                </div>
                <div class="home_page_text_hidden">
                    <p class="home_page_h1_text">Today.</p>
                </div>
            </div>
            <div class="home_page_solar_img_container">
                <img class="home_page_solar_panel" src="{{ asset('images/static/solar panel.webp') }}" alt="">
                <img class="home_page_image home_page_img_1 home_page_outsole"
                     src="{{ asset('images/static/Layer 1.webp') }}" alt="">
                <img class="home_page_image home_page_img_2 home_page_outsole"
                     src="{{ asset('images/static/Layer 2.webp') }}" alt="">
                <img class="home_page_image home_page_img_3 home_page_outsole"
                     src="{{ asset('images/static/Layer 3.webp') }}" alt="">
                <img class="home_page_image home_page_img_4 home_page_outsole"
                     src="{{ asset('images/static/Layer 4.webp') }}" alt="">
                <img class="home_page_image home_page_img_5 home_page_outsole"
                     src="{{ asset('images/static/Layer 5.webp') }}" alt="">
                <img class="home_page_image home_page_img_6 home_page_outsole"
                     src="{{ asset('images/static/Layer 6.webp') }}" alt="">
                <img class="home_page_image home_page_img_7 home_page_outsole"
                     src="{{ asset('images/static/Layer 7.webp') }}" alt="">
                <img class="home_page_image home_page_img_8 home_page_outsole"
                     src="{{ asset('images/static/Layer 8.webp') }}" alt="">
                <img class="home_page_image home_page_img_9 home_page_outsole"
                     src="{{ asset('images/static/Layer 9.webp') }}" alt="">
                <img class="home_page_image home_page_img_10 home_page_outsole"
                     src="{{ asset('images/static/Layer 10.webp') }}" alt="">
            </div>
        </div>
    </section>
    <section class="home_page_section_2" id="section_2">
        <div class="home_page_sec_2_text_info">
            @foreach($contents->get('home','section_2') as $section2)
                <div class="home_page_sec_2_text_cont">
                    <div class="home_page_text_hidden">
                        <p class="home_page_sec_2_text_h4">{{ $section2?->title }}</p>
                    </div>
                </div>
                <div class="home_page_sec_2_text_cont_tablet">
                    <div class="home_page_text_hidden">
                        <p class="home_page_sec_2_text_h4">Your decision matters,</p>
                    </div>
                    <div class="home_page_text_hidden">
                        <p class="home_page_sec_2_text_h4">choose wisely.</p>
                    </div>
                </div>
                <div class="home_page_sec_2_text_cont_2">
                    <p class="home_page_sec_2_text_p">{!! $section2?->content !!}</p>
                </div>
            @endforeach
        </div>
        <div class="home_page_sec_2_img_cont">
            <img class="home_page_sec_2_img" src="{{ asset('images/static/Rectangle 17.webp') }}" alt="">
        </div>
    </section>
    <section class="home_page_section_3">
        <div class="home_page_image_home">
            @foreach($contents->get('home','section_3') as $key => $section3)
                <span class="home_page_pulse_{{$key + 1}}"></span>
                <div class="home_page_pulse_info_{{$key + 1}}">
                    <p class="home_page_pulse_info_1_p_1">{{ $section3->title }}</p>
                    <p class="home_page_pulse_info_1_p_2">{{ $section3->content }}</p>
                    <div class="home_page_pulse_info_img">
                        <img class="home_page_pulse_img" src="{{ $section3->mainImage->urls['small'] }}" alt="">
                    </div>
                </div>
            @endforeach
{{--            <span class="home_page_pulse_1"></span>--}}
{{--            <div class="home_page_pulse_info_1">--}}
{{--                <p class="home_page_pulse_info_1_p_1">Lorem ipsum</p>--}}
{{--                <p class="home_page_pulse_info_1_p_2">Lorem ipsum dolor sit amet consectetur. Fermentum praesent in--}}
{{--                    non</p>--}}
{{--                <div class="home_page_pulse_info_img">--}}
{{--                    <img class="home_page_pulse_img" src="{{ asset('images/static/image 9.webp') }}" alt="">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <span class="home_page_pulse_2"></span>--}}
{{--            <div class="home_page_pulse_info_2">--}}
{{--                <p class="home_page_pulse_info_1_p_1">Lorem ipsum</p>--}}
{{--                <p class="home_page_pulse_info_1_p_2">Lorem ipsum dolor sit amet consectetur. Fermentum praesent in--}}
{{--                    non</p>--}}
{{--                <div class="home_page_pulse_info_img">--}}
{{--                    <img class="home_page_pulse_img" src="{{ asset('images/static/image 9.webp') }}" alt="">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <span class="home_page_pulse_3"></span>--}}
{{--            <div class="home_page_pulse_info_3">--}}
{{--                <p class="home_page_pulse_info_1_p_1">Lorem ipsum</p>--}}
{{--                <p class="home_page_pulse_info_1_p_2">Lorem ipsum dolor sit amet consectetur. Fermentum praesent in--}}
{{--                    non</p>--}}
{{--                <div class="home_page_pulse_info_img">--}}
{{--                    <img class="home_page_pulse_img" src="{{ asset('images/static/image 9.webp') }}" alt="">--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </section>
    <section class="home_page_section_4">
        <div class="image-scale section image-scale__section">
            <div class="divider"></div>
            <div class="box box one box--one"></div>
            <div class="box box two box--two"></div>
            <div class="box box three box--three"></div>
            <div class="box box four box--four"></div>
            <div class="container">
                <div class="image-scale">
                    <div class="image-scale left">
                        <div class="home_page_sec_4_texts_cont">
                            @foreach($contents->get('home', 'section_4') as $section4)
                                <div class="home_page_texts_cont">
                                    <p class="home_page_sec_4_p_1">{{ $section4->title }}</p>
                                    <p class="home_page_sec_4_h3">{{ $section4->subtitle }}</p>
                                    {!! $section4->content !!}
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="image-scale right image-scale__right">
                        <img class="home_page_image_scroll" src="{{ asset('images/static/Frame 1000004452.webp') }}"/>
                    </div>
                </div>
            </div>
        </div>
    </section>
        <section class="home_page_section_4_tablet_mobile">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="home_page_texts_cont">
                            <p class="home_page_sec_4_p_1">FEDERAL TAX CREDIT</p>
                            <h3 class="home_page_sec_4_h3">30% remaining </h3>
                            <p class="home_page_sec_4_p_2">You hold the power to secure your financial future and also make a
                                positive impact on the planet.</p>
                            <p class="home_page_sec_4_p_2">With over a decade of experience in the solar industry,
                                collaborating
                                with us promises a boutique experience, complete with the outstanding resources and
                                of a
                                major corporation.</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="home_page_texts_cont">
                            <p class="home_page_sec_4_p_1">FEDERAL TAX CREDIT</p>
                            <h3 class="home_page_sec_4_h3">Solar and Save</h3>
                            <p class="home_page_sec_4_p_2">You hold the power to secure your financial future and also make a
                                positive impact on the planet.</p>
                            <p class="home_page_sec_4_p_2">With over a decade of experience in the solar industry,
                                collaborating
                                with us promises a boutique experience, complete with the outstanding resources and
                                of a
                                major corporation.</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="home_page_texts_cont">
                            <p class="home_page_sec_4_p_1">FEDERAL TAX CREDIT</p>
                            <h3 class="home_page_sec_4_h3">Solar with Confidence</h3>
                            <p class="home_page_sec_4_p_2">You hold the power to secure your financial future and also make a
                                positive impact on the planet.</p>
                            <p class="home_page_sec_4_p_2">With over a decade of experience in the solar industry,
                                collaborating
                                with us promises a boutique experience, complete with the outstanding resources and
                                of a
                                major corporation.</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="home_page_texts_cont">
                            <p class="home_page_sec_4_p_1">FEDERAL TAX CREDIT</p>
                            <h3 class="home_page_sec_4_h3">Own Your Power</h3>
                            <p class="home_page_sec_4_p_2">You hold the power to secure your financial future and also make a
                                positive impact on the planet.</p>
                            <p class="home_page_sec_4_p_2">With over a decade of experience in the solar industry,
                                collaborating
                                with us promises a boutique experience, complete with the outstanding resources and
                                of a
                                major corporation.</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="home_page_texts_cont">
                            <p class="home_page_sec_4_p_1">FEDERAL TAX CREDIT</p>
                            <h3 class="home_page_sec_4_h3">Maximize Your Solar Investment</h3>
                            <p class="home_page_sec_4_p_2">You hold the power to secure your financial future and also make a
                                positive impact on the planet.</p>
                            <p class="home_page_sec_4_p_2">With over a decade of experience in the solar industry,
                                collaborating
                                with us promises a boutique experience, complete with the outstanding resources and
                                of a
                                major corporation.</p>
                        </div>
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
            <div class="home_page_sec_4_tablet_mobile_img_cont">
                <img class="home_page_sec_4_tablet_mobile_image" src="{{ asset('images/static/Rectangle 46.webp') }}" alt="">
            </div>
        </section>
        <section class="home_page_section_5">
            <div class="home_page_sec_5_hidden">
                <div class="home_page_sec_5_img_cont">
                    <img class="home_page_image_scroll_2" src="{{ asset('images/static/Ellipse 203.webp') }}" alt="">
                </div>
                <div class="home_page_sec_5_texts_cont">
                    @foreach($contents->get('home', 'section_5') as $section5)
                        <div class="home_page_sec_5_text_cont home_page_texts">
                            <p class="home_page_sec_5_text_p">{{ $section5->subtitle }}</p>
                            <p class="home_page_sec_5_text_p_2">{{ $section5->content }}</p>
                            <p class="home_page_sec_5_text_h4">{{ $section5->title }} </p>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="home_page_sec_5_hidden_tablet">
                <div class="home_page_sec_5_texts_cont">
                    @foreach($contents->get('home', 'section_5') as $section5)
                        <div class="home_page_sec_5_text_cont home_page_texts">
                            <p class="home_page_sec_5_text_p">{{ $section5->subtitle }}</p>
                            <p class="home_page_sec_5_text_p_2">{{ $section5->content }}</p>
                            <p class="home_page_sec_5_text_h4">{{ $section5->title }} </p>
                        </div>
                    @endforeach

            </div>
            <div class="home_page_sec_5_img_cont">
                <img class="home_page_image_scroll_2" src="{{ asset('images/static/Ellipse 203.webp') }}" alt="">
            </div>
        </div>
        </section>
{{--   foreach petqa frral mobilei hamar     --}}
        <section class="home_page_sec_5_mobile_cont">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide swiper-slide-2">
                        <div class="home_page_texts_cont">
                            <p class="home_page_sec_5_text_p">INFLATION</p>
                            <p class="home_page_sec_5_text_p_2">With our solar system, we offer comprehensive protection against
                                inflation
                                because of its ability to maintain its value and return on investment within 4-6 years.
                            </p>
                            <p class="home_page_sec_5_text_h4">Predictable energy costs</p>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-2">
                        <div class="home_page_texts_cont">
                            <p class="home_page_sec_5_text_p">INFLATION</p>
                            <p class="home_page_sec_5_text_p_2">We are pleased to offer several financing options, each designed
                                to
                                provide
                                our esteemed clients with a high degree of flexibility, affordability, and security.</p>
                            <p class="home_page_sec_5_text_h4">FINANCING MADE FLEXIBLE</p>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-2">
                        <div class="home_page_texts_cont">
                            <p class="home_page_sec_5_text_p">INFLATION</p>
                            <p class="home_page_sec_5_text_p_2">Our current specials provide unparalleled value, including no down
                                payment,
                                zero monthly payments for the Initial 12 months, and a $0 electric bill, ensuring that
                                clients
                                can
                                seamlessly transition to sustainable energy without undue financial burden.</p>
                            <p class="home_page_sec_5_text_h4">THE VALUE IS UNMATCHABLE</p>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-2">
                        <div class="home_page_texts_cont">
                            <p class="home_page_sec_5_text_p">INFLATION</p>
                            <p class="home_page_sec_5_text_p_2">Alternatively, our clients may opt for our bank-financing plan,
                                with the
                                bank
                                providing 100% of the loan to our solar company, as well as a generous 18-month term.
                                Following
                                the
                                initial 12 months of no payments, clients will then pay 70% of the remaining balance to
                                the
                                bank,
                                applying the remaining 30% from federal tax credit.</p>
                            <p class="home_page_sec_5_text_h4">LET THE BANK MAKE THE FIRST MOVE</p>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-2">
                        <div class="home_page_texts_cont">
                                                            <p class="home_page_sec_5_text_p">INFLATION</p>
                                                            <p class="home_page_sec_5_text_p_2">Alternatively, our clients may opt for our bank-financing plan,
                                                                with the
                                                                bank
                                                                providing 100% of the loan to our solar company, as well as a generous 18-month term.
                                                                Following
                                                                the
                                                                initial 12 months of no payments, clients will then pay 70% of the remaining balance to
                                                                the
                                                                bank,
                                                                applying the remaining 30% from federal tax credit.</p>
                                                            <p class="home_page_sec_5_text_h4">STUBBORN RATES THAT SAVE YOU MONEY</p>
                                                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-2">
                        <div class="home_page_texts_cont">
                            <p class="home_page_sec_5_text_p">INFLATION</p>
                            <p class="home_page_sec_5_text_p_2">Alternatively, our clients may opt for our bank-financing plan,
                                with the
                                bank
                                providing 100% of the loan to our solar company, as well as a generous 18-month term.
                                Following
                                the
                                initial 12 months of no payments, clients will then pay 70% of the remaining balance to
                                the
                                bank,
                                applying the remaining 30% from federal tax credit.</p>
                            <p class="home_page_sec_5_text_h4">BOOSTING VALUE THROUGH THE ROOF</p>
                        </div>
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
            <div class="home_page_sec_5_img_cont home_page_sec_5_img_cont2">
                <img class="home_page_image_circle" src="{{ asset('images/static/Ellipse 203.webp') }}" alt="">
            </div>
        </section>
{{--   end foreach petqa frral mobilei hamar end     --}}
        <section class="home_page_section_6">
            <div class="home_page_sec_6_texts_cont">
                <div class="home_page_sec_6_text_cont">
                    <p class="home_page_sec_6_p">Consultation</p>
                    <p class="home_page_sec_6_h6">1 day</p>
                </div>
                <div class="home_page_sec_6_text_cont">
                    <p class="home_page_sec_6_p">Service & financing agreement</p>
                    <p class="home_page_sec_6_h6">3 days</p>
                </div>
                <div class="home_page_sec_6_text_cont">
                    <p class="home_page_sec_6_p">Site survey</p>
                    <p class="home_page_sec_6_h6">1 day</p>
                </div>
                <div class="home_page_sec_6_text_cont">
                    <p class="home_page_sec_6_p">Final design</p>
                    <p class="home_page_sec_6_h6">3 days</p>
                </div>
                <div class="home_page_sec_6_text_cont">
                    <p class="home_page_sec_6_p">permitting</p>
                    <p class="home_page_sec_6_h6">7-14 days</p>
                </div>
                <div class="home_page_sec_6_text_cont">
                    <p class="home_page_sec_6_p">installation</p>
                    <p class="home_page_sec_6_h6">2-3 days</p>
                </div>
                <div class="home_page_sec_6_text_cont">
                    <p class="home_page_sec_6_p">inspection</p>
                    <p class="home_page_sec_6_h6">5-10 days</p>
                </div>
                <div class="home_page_sec_6_text_cont">
                    <p class="home_page_sec_6_p">utility connection</p>
                    <p class="home_page_sec_6_h6">2-4 weeks</p>
                </div>
                <div class="home_page_sec_6_text_cont">
                    <p class="home_page_sec_6_p">Monitoring activation</p>
                    <p class="home_page_sec_6_h6">Final step</p>
                </div>
            </div>
            <div class="home_page_sec_6_img_cont">
                <svg class="home_page_svg_color_blue" width="394" height="394" viewBox="0 0 394 394" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <circle cx="197" cy="197" r="197" fill="#1931AF" />
                </svg>
                <div class="home_page_sec_6_image_cont">
                    <div class="home_page_image_cont_sec_6">
                        <img class="home_page_sec_6_image" src="{{ asset('images/static/panel-form.webp') }}" alt="">
                    </div>
                    <div class="home_page_image_cont_sec_6">
                        <img class="home_page_sec_6_image" src="{{ asset('images/static/money - form.webp') }}" alt="">
                    </div>
                    <div class="home_page_image_cont_sec_6">
                        <img class="home_page_sec_6_image" src="{{ asset('images/static/tablet-form.webp') }}" alt="">
                    </div>
                    <div class="home_page_image_cont_sec_6">
                        <img class="home_page_sec_6_image" src="{{ asset('images/static/home-flash-form.webp') }}" alt="">
                    </div>
                    <div class="home_page_image_cont_sec_6">
                        <img class="home_page_sec_6_image" src="{{ asset('images/static/home-wifi-from.webp') }}" alt="">
                    </div>
                    <div class="home_page_image_cont_sec_6">
                        <img class="home_page_sec_6_image" src="{{ asset('images/static/pen-form.webp') }}" alt="">
                    </div>
                    <div class="home_page_image_cont_sec_6">
                        <img class="home_page_sec_6_image" src="{{ asset('images/static/check-form.webp') }}" alt="">
                    </div>
                    <div class="home_page_image_cont_sec_6">
                        <img class="home_page_sec_6_image" src="{{ asset('images/static/loop-form.webp') }}" alt="">
                    </div>
                    <div class="home_page_image_cont_sec_6">
                        <img class="home_page_sec_6_image" src="{{ asset('images/static/pie-chart-form.webp') }}" alt="">
                    </div>
                </div>
                <div class="home_page_left_hand">
                    <img class="home_page_image_hand" src="{{ asset('images/static/2unsplash_36g19a7ivrs.webp') }}" alt="">
                </div>
                <div class="home_page_right_hand">
                    <img class="home_page_image_hand" src="{{ asset('images/static/2unsplash_36g19a7ivrs (1).') }}webp" alt="">
                </div>
            </div>
        </section>
        <section class="home_page_section_6_tablet_mobile">
            <svg class="svg_color_blue2" width="200" height="200" viewBox="0 0 394 394" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
                <circle cx="197" cy="197" r="197" fill="#1931AF" />
            </svg>
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide swiper-slide-3">
                        <div class="home_page_swiper-slide-img-cont3">
                            <div class="home_page_sec_6_text_cont">
                                <p class="home_page_sec_6_p">Consultation</p>
                                <p class="home_page_sec_6_h6">1 day</p>
                            </div>
                            <div class="home_page_image_cont_sec_6">
                                <img class="home_page_sec_6_image" src="{{ asset('images/static/panel-form.webp') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-3">
                        <div class="home_page_swiper-slide-img-cont3">
                            <div class="home_page_sec_6_text_cont">
                                <p class="home_page_sec_6_p">Service & financing agreement</p>
                                <p class="home_page_sec_6_h6">3 days</p>
                            </div>
                            <div class="home_page_image_cont_sec_6">
                                <img class="home_page_sec_6_image" src="{{ asset('images/static/money - form.webp') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-3">
                        <div class="home_page_swiper-slide-img-cont3">
                            <div class="home_page_sec_6_text_cont">
                                <p class="home_page_sec_6_p">Site survey</p>
                                <p class="home_page_sec_6_h6">1 day</p>
                            </div>
                            <div class="home_page_image_cont_sec_6">
                                <img class="home_page_sec_6_image" src="{{ asset('images/static/tablet-form.webp') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-3">
                        <div class="home_page_swiper-slide-img-cont3">
                            <div class="home_page_sec_6_text_cont">
                                <p class="home_page_sec_6_p">Final design</p>
                                <p class="home_page_sec_6_h6">3 days</p>
                            </div>
                            <div class="home_page_image_cont_sec_6">
                                <img class="home_page_sec_6_image" src="{{ asset('images/static/home-flash-form.webp') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-3">
                        <div class="home_page_swiper-slide-img-cont3">
                            <div class="home_page_sec_6_text_cont">
                                <p class="home_page_sec_6_p">permitting</p>
                                <p class="home_page_sec_6_h6">7-14 days</p>
                            </div>
                            <div class="home_page_image_cont_sec_6">
                                <img class="home_page_sec_6_image" src="{{ asset('images/static/home-wifi-from.webp') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-3">
                        <div class="home_page_swiper-slide-img-cont3">
                            <div class="home_page_sec_6_text_cont">
                                <p class="home_page_sec_6_p">installation</p>
                                <p class="home_page_sec_6_h6">2-3 days</p>
                            </div>
                            <div class="home_page_image_cont_sec_6">
                                <img class="home_page_sec_6_image" src="{{ asset('images/static/pen-form.webp') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-3">
                        <div class="home_page_swiper-slide-img-cont3">
                            <div class="home_page_sec_6_text_cont">
                                <p class="home_page_sec_6_p">inspection</p>
                                <p class="home_page_sec_6_h6">5-10 days</p>
                            </div>
                            <div class="home_page_image_cont_sec_6">
                                <img class="home_page_sec_6_image" src="{{ asset('images/static/check-form.webp') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-3">
                        <div class="home_page_swiper-slide-img-cont3">
                            <div class="home_page_sec_6_text_cont">
                                <p class="home_page_sec_6_p">utility connection</p>
                                <p class="home_page_sec_6_h6">2-4 weeks</p>
                            </div>
                            <div class="home_page_image_cont_sec_6">
                                <img class="home_page_sec_6_image" src="{{ asset('images/static/loop-form.webp') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide swiper-slide-3">
                        <div class="home_page_swiper-slide-img-cont3">
                            <div class="home_page_sec_6_text_cont">
                                <p class="home_page_sec_6_p">Monitoring activation</p>
                                <p class="home_page_sec_6_h6">Final step</p>
                            </div>
                            <div class="home_page_image_cont_sec_6">
                                <img class="home_page_sec_6_image" src="{{ asset('images/static/pie-chart-form.webp') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
            <div class="home_page_left_hand">
                <img class="home_page_image_hand" src="{{ asset('images/static/2unsplash_36g19a7ivrs.webp') }}" alt="">
            </div>
            <div class="home_page_right_hand">
                <img class="home_page_image_hand" src="{{ asset('images/static/2unsplash_36g19a7ivrs (1).webp') }}" alt="">
            </div>



        </section>
        <section class="home_page_section_7">
            <div class="home_page_sec_7_img_cont">
                <img class="home_page_sec_7_img" src="{{ asset('images/static/Rectangle 32.webp') }}" alt="">
            </div>
            <div class="home_page_sec_7_text_cont">
                @foreach($contents->get('home', 'section_7') as $section7)
                    <div class="home_page_text_hidden">
                        <p class="home_page_sec_7_h4">{{ $section7->title }}</p>
                    </div>
                    <p class="home_page_sec_7_p"> {!! $section7->content !!}</p>
                @endforeach
            </div>
        </section>
        <section class="home_page_section_7_tablet_mobile">
            <div class="home_page_sec_7_tablet_mobile_text_cont">
                @foreach($contents->get('home', 'section_7') as $section7)
                    <div class="home_page_text_hidden">
                        <p class="home_page_sec_7_h4">{{ $section7->title }}</p>
                    </div>
                    {!! $section7->content !!}
                @endforeach
            </div>
            <div class="home_page_sec_7_tablet_mobile_img_cont">
                <img class="home_page_sec_7_tablet_mobile_img" src="{{ asset('images/static/Rectangle 32_tablet.webp') }}" alt="">
            </div>
        </section>
        <section class="home_page_section_8">
            <div class="home_page_sec_8_text_cont">
                <div class="home_page_text_hidden">
                    <p class="home_page_sec_8_h4">INDUSTRY LEADING</p>
                </div>
                <div class="home_page_text_hidden">
                    <p class="home_page_sec_8_h4">WARRANTIES</p>
                </div>
            </div>
            <div class="home_page_sec_8_icons_cont">
                <div class="home_page_icons_text">
                    <div class="home_page_icon">
                        <svg width="124" height="112" viewBox="0 0 124 112" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="37.5233" height="53.3886" fill="white" />
                            <rect x="86" width="37.5233" height="53.3886" fill="white" />
                            <rect y="58.6108" width="37.5233" height="53.3886" fill="white" />
                            <rect x="42.8242" y="37.1396" width="37.5233" height="74.8602" fill="#FFF353" />
                            <rect x="42.8242" y="0.580078" width="37.5233" height="31.3368" fill="#FFF353" />
                            <rect x="86" y="58.6108" width="37.5233" height="53.3886" fill="white" />
                        </svg>
                    </div>
                    <div class="home_page_icon_text">25 year warranty on roof</div>
                </div>
                <div class="home_page_icons_text">
                    <div class="home_page_icon_2">
                        <svg width="153" height="153" viewBox="0 0 153 153" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="76.5001" cy="76.5001" r="45.2755" fill="#FFF353" />
                            <rect x="68.6924" y="130.362" width="15.6122" height="22.6377" fill="white" />
                            <rect x="68.6924" width="15.6122" height="22.6377" fill="white" />
                            <rect x="42.8086" y="119.242" width="15.6122" height="22.6377"
                                  transform="rotate(30 42.8086 119.242)" fill="white" />
                            <rect x="107.988" y="6.34521" width="15.6122" height="22.6377"
                                  transform="rotate(30 107.988 6.34521)" fill="white" />
                            <rect x="25.9502" y="96.6704" width="15.6122" height="22.6377"
                                  transform="rotate(60 25.9502 96.6704)" fill="white" />
                            <rect x="138.847" y="31.4893" width="15.6122" height="22.6377"
                                  transform="rotate(60 138.847 31.4893)" fill="white" />
                            <rect x="22.6377" y="68.6938" width="15.6122" height="22.6377"
                                  transform="rotate(90 22.6377 68.6938)" fill="white" />
                            <rect x="153" y="68.6938" width="15.6122" height="22.6377" transform="rotate(90 153 68.6938)"
                                  fill="white" />
                            <rect x="33.7568" y="42.8081" width="15.6122" height="22.6377"
                                  transform="rotate(120 33.7568 42.8081)" fill="white" />
                            <rect x="146.653" y="107.989" width="15.6122" height="22.6377"
                                  transform="rotate(120 146.653 107.989)" fill="white" />
                            <rect x="56.3301" y="25.9521" width="15.6122" height="22.6377"
                                  transform="rotate(150 56.3301 25.9521)" fill="white" />
                            <rect x="121.511" y="138.849" width="15.6122" height="22.6377"
                                  transform="rotate(150 121.511 138.849)" fill="white" />
                        </svg>
                    </div>
                    <div class="home_page_icon_text">25 year warranty on solar system production</div>
                </div>
                <div class="home_page_icons_text">
                    <div class="home_page_icon_3">
                        <svg width="145" height="107" viewBox="0 0 145 107" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M139.642 83.4375H6.54246C4.4899 54.9742 18.3461 28.302 40.6517 14.8018L45.352 56.8599C45.6913 59.9137 48.4448 62.1387 51.493 61.7716C54.5524 61.4267 56.7496 58.6788 56.4047 55.625L50.8756 6.16325C50.7143 4.60575 51.2205 3.04269 52.2607 1.89125C53.3509 0.689751 54.9084 0 56.5549 0H89.6295C91.2705 0 92.8335 0.689751 93.9182 1.89125C94.964 3.04825 95.4702 4.5835 95.3144 6.06869L89.7797 55.625C89.4348 58.6788 91.632 61.4267 94.6914 61.766C97.7174 62.122 100.493 59.9248 100.832 56.8543L105.533 14.7962C127.838 28.302 141.695 54.9742 139.642 83.4375Z"
                                fill="white" />
                            <rect y="93" width="145" height="14" fill="#FFF353" />
                        </svg>
                    </div>
                    <div class="home_page_icon_text">25 year warranty on labor</div>
                </div>
                <div class="home_page_icons_text">
                    <div class="home_page_icon_4">
                        <svg width="160" height="160" viewBox="0 0 160 160" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_1066_5311)">
                                <rect x="18" y="27" width="37.5233" height="53.3886" fill="white" />
                                <rect x="18" y="85.6108" width="37.5233" height="53.3886" fill="white" />
                                <rect x="60.8242" y="64.1396" width="37.5233" height="74.8602" fill="#FFF353" />
                                <rect x="60.8242" y="27.5801" width="37.5233" height="31.3368" fill="#FFF353" />
                                <rect x="104" y="85.6108" width="37.5233" height="53.3886" fill="white" />
                                <circle cx="162.754" cy="22.7498" r="34.7703" fill="#FFF353" />
                                <rect x="156.755" y="64.1152" width="11.9898" height="17.3852" fill="white" />
                                <rect x="136.878" y="55.5752" width="11.9898" height="17.3852"
                                      transform="rotate(30 136.878 55.5752)" fill="white" />
                                <rect x="123.93" y="38.2407" width="11.9898" height="17.3852"
                                      transform="rotate(60 123.93 38.2407)" fill="white" />
                                <rect x="121.385" y="16.7554" width="11.9898" height="17.3852"
                                      transform="rotate(90 121.385 16.7554)" fill="white" />
                                <rect x="129.926" y="-3.12402" width="11.9898" height="17.3852"
                                      transform="rotate(120 129.926 -3.12402)" fill="white" />
                            </g>
                            <defs>
                                <clipPath id="clip0_1066_5311">
                                    <rect width="160" height="160" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                    </div>
                    <div class="home_page_icon_text">25 year warranty on solar system</div>
                </div>
            </div>
    </section>
    <!--        this is home_page_section_9-->
    <section>
        <home-page-chart
            :prop-per-solar="{{ json_encode($settings->get('chart_per_solar')) }}"
            :prop-post-solar="{{ json_encode($settings->get('chart_post_solar')) }}"
            :prop-years="{{ json_encode($settings->get('chart_years')) }}"
        />
    </section>
    <!--        -->
    <section class="home_page_section_10">
        <div class="home_page_sec_10_text_cont">
            @foreach($contents->get('home', 'section_10') as $section10)
                <div class="home_page_text_hidden">
                    <p class="home_page_sec_10_h4">{{ $section10->title }}</p>
                    <p class="home_page_sec_10_h4">{{ $section10->subtitle }}</p>
                </div>
                <p class="home_page_sec_10_p">{!! $section10->content !!}</p>
            @endforeach
            <button class="home_page_sec_10_btn">
                <a class="home_page_sec_10_btn_p" href="#">Lore more</a>
            </button>
        </div>
        <div class="home_page_parallax_cont">
            <div class="home_page_sec_10_img_cont">
                <div class="home_page_parallax_img"></div>
            </div>
        </div>
    </section>
        <section class="home_page_section_10_tablet_mobile">
            <div class="home_page_sec_10_tablet_clip_path_cont">
                <div class="home_page_parallax_cont">
                    <div class="home_page_sec_10_img_cont">
                        <div class="home_page_parallax_img"></div>
                    </div>
                </div>
            </div>
            <div class="home_page_sec_10_tablet_text_cont">
                <div class="home_page_text_hidden">
                    <p class="home_page_sec_10_h4">WARRANTIES</p>
                </div>
                <p class="home_page_sec_10_p">Partnering for Peace of Mind: Our commitment to excellence extends to the partners we
                    work with. We only collaborate with companies that offer a 25-year warranty, ensuring our clients' peace
                    of mind for the long-term. Additionally, we proudly partner with the industry's leading technology
                    provider, Enphase, offering top-of-the-line micro-inverters for exceptional performance and reliability.
                </p>
                <button class="home_page_sec_10_btn">
                    <a class="home_page_sec_10_btn_p" href="#">Lore more</a>
                </button>
            </div>
        </section>
        <section class="home_page_section_11">
            <div class="home_page_sec_11_text_cont">
                <div class="home_page_text_hidden">
                    <p class="home_page_sec_11_h4">What Our Clients Say</p>
                </div>
                <div class="home_page_text_hidden">
                    <p class="home_page_sec_11_h4">About Our Work</p>
                </div>
            </div>
            <div class="container">
                <div class="flex">
                    <div class="content">
                        <div class="home_page_video_text_cont_1">
                            <div class="single-portfolio">
                                <svg class="cursor" width="114" height="114" viewBox="0 0 114 114" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="57" cy="57" r="57" fill="#FFFDFD" fill-opacity="0.55" />
                                    <path d="M73 55.5L49.75 68.0574L49.75 42.9426L73 55.5Z" fill="#402B2B" />
                                </svg>
                                <img class="video" src="{{ asset("images/static/Rectangle 13.webp") }}" alt="">
                                <div class="full">
                                    <div class="center">
                                        <div class="close">
                                            <svg class="close-color" height="80px" fill="blue" version="1.1"
                                                 viewBox="0 0 512 512" width="80px" xml:space="preserve"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <path
                                                d="M443.6,387.1L312.4,255.4l131.5-130c5.4-5.4,5.4-14.2,0-19.6l-37.4-37.6c-2.6-2.6-6.1-4-9.8-4c-3.7,0-7.2,1.5-9.8,4  L256,197.8L124.9,68.3c-2.6-2.6-6.1-4-9.8-4c-3.7,0-7.2,1.5-9.8,4L68,105.9c-5.4,5.4-5.4,14.2,0,19.6l131.5,130L68.4,387.1  c-2.6,2.6-4.1,6.1-4.1,9.8c0,3.7,1.4,7.2,4.1,9.8l37.4,37.6c2.7,2.7,6.2,4.1,9.8,4.1c3.5,0,7.1-1.3,9.8-4.1L256,313.1l130.7,131.1  c2.7,2.7,6.2,4.1,9.8,4.1c3.5,0,7.1-1.3,9.8-4.1l37.4-37.6c2.6-2.6,4.1-6.1,4.1-9.8C447.7,393.2,446.2,389.7,443.6,387.1z" />
                                        </svg>
                                        </div>
                                        <div class="chech_video">
                                            <iframe width="100%" height="100%"
                                                    src="https://www.youtube.com/embed/FkqJ-EnKUxM?list=PLOQDek48BpZFzxKS-2sLAeSBB83x-xILB"
                                                    title="Урок GSAP 3.6 - Начало работы - Эпизод 1" frameborder="0"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                                    allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="home_page_sec_11_p_1">Dave Goldblatt</div>
                            <div class="home_page_sec_11_p_2">CEO of Wavecut</div>
                        </div>
                    </div>
                    <div class="content">
                        <div class="home_page_video_text_cont_1">
                            <div class="single-portfolio">
                                <svg class="cursor" width="114" height="114" viewBox="0 0 114 114" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="57" cy="57" r="57" fill="#FFFDFD" fill-opacity="0.55" />
                                    <path d="M73 55.5L49.75 68.0574L49.75 42.9426L73 55.5Z" fill="#402B2B" />
                                </svg>
                                <img class="video" src="{{ asset("images/static/Rectangle 13.webp") }}" alt="">
                                <div class="full">
                                    <div class="center">
                                        <div class="close">
                                            <svg class="close-color" height="80px" fill="blue" version="1.1"
                                                 viewBox="0 0 512 512" width="80px" xml:space="preserve"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <path
                                                d="M443.6,387.1L312.4,255.4l131.5-130c5.4-5.4,5.4-14.2,0-19.6l-37.4-37.6c-2.6-2.6-6.1-4-9.8-4c-3.7,0-7.2,1.5-9.8,4  L256,197.8L124.9,68.3c-2.6-2.6-6.1-4-9.8-4c-3.7,0-7.2,1.5-9.8,4L68,105.9c-5.4,5.4-5.4,14.2,0,19.6l131.5,130L68.4,387.1  c-2.6,2.6-4.1,6.1-4.1,9.8c0,3.7,1.4,7.2,4.1,9.8l37.4,37.6c2.7,2.7,6.2,4.1,9.8,4.1c3.5,0,7.1-1.3,9.8-4.1L256,313.1l130.7,131.1  c2.7,2.7,6.2,4.1,9.8,4.1c3.5,0,7.1-1.3,9.8-4.1l37.4-37.6c2.6-2.6,4.1-6.1,4.1-9.8C447.7,393.2,446.2,389.7,443.6,387.1z" />
                                        </svg>
                                        </div>
                                        <div class="chech_video">
                                            <iframe width="100%" height="100%"
                                                    src="https://www.youtube.com/embed/FkqJ-EnKUxM?list=PLOQDek48BpZFzxKS-2sLAeSBB83x-xILB"
                                                    title="Урок GSAP 3.6 - Начало работы - Эпизод 1" frameborder="0"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                                    allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="home_page_sec_11_p_1">Dave Goldblatt</div>
                            <div class="home_page_sec_11_p_2">CEO of Wavecut</div>
                        </div>
                    </div>
                    <div class="content">
                        <div class="home_page_video_text_cont_1">
                            <div class="single-portfolio">
                                <svg class="cursor" width="114" height="114" viewBox="0 0 114 114" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="57" cy="57" r="57" fill="#FFFDFD" fill-opacity="0.55" />
                                    <path d="M73 55.5L49.75 68.0574L49.75 42.9426L73 55.5Z" fill="#402B2B" />
                                </svg>
                                <img class="video" src="{{ asset("images/static/Rectangle 13.webp") }}" alt="">
                                <div class="full">
                                    <div class="center">
                                        <div class="close">
                                            <svg class="close-color" height="80px" fill="blue" version="1.1"
                                                 viewBox="0 0 512 512" width="80px" xml:space="preserve"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <path
                                            d="M443.6,387.1L312.4,255.4l131.5-130c5.4-5.4,5.4-14.2,0-19.6l-37.4-37.6c-2.6-2.6-6.1-4-9.8-4c-3.7,0-7.2,1.5-9.8,4  L256,197.8L124.9,68.3c-2.6-2.6-6.1-4-9.8-4c-3.7,0-7.2,1.5-9.8,4L68,105.9c-5.4,5.4-5.4,14.2,0,19.6l131.5,130L68.4,387.1  c-2.6,2.6-4.1,6.1-4.1,9.8c0,3.7,1.4,7.2,4.1,9.8l37.4,37.6c2.7,2.7,6.2,4.1,9.8,4.1c3.5,0,7.1-1.3,9.8-4.1L256,313.1l130.7,131.1  c2.7,2.7,6.2,4.1,9.8,4.1c3.5,0,7.1-1.3,9.8-4.1l37.4-37.6c2.6-2.6,4.1-6.1,4.1-9.8C447.7,393.2,446.2,389.7,443.6,387.1z" />
                                    </svg>
                                        </div>
                                        <div class="chech_video">
                                            <iframe width="100%" height="100%"
                                                    src="https://www.youtube.com/embed/FkqJ-EnKUxM?list=PLOQDek48BpZFzxKS-2sLAeSBB83x-xILB"
                                                    title="Урок GSAP 3.6 - Начало работы - Эпизод 1" frameborder="0"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                                    allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="home_page_sec_11_p_1">Dave Goldblatt</div>
                            <div class="home_page_sec_11_p_2">CEO of Wavecut</div>
                        </div>
                    </div>
                    <div class="content">
                        <div class="home_page_video_text_cont_1">
                            <div class="single-portfolio">
                                <svg class="cursor" width="114" height="114" viewBox="0 0 114 114" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="57" cy="57" r="57" fill="#FFFDFD" fill-opacity="0.55" />
                                    <path d="M73 55.5L49.75 68.0574L49.75 42.9426L73 55.5Z" fill="#402B2B" />
                                </svg>
                                <img class="video" src="{{ asset("images/static/Rectangle 13.webp") }}" alt="">
                                <div class="full">
                                    <div class="center">
                                        <div class="close">
                                            <svg class="close-color" height="80px" fill="blue" version="1.1"
                                                 viewBox="0 0 512 512" width="80px" xml:space="preserve"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <path
                                            d="M443.6,387.1L312.4,255.4l131.5-130c5.4-5.4,5.4-14.2,0-19.6l-37.4-37.6c-2.6-2.6-6.1-4-9.8-4c-3.7,0-7.2,1.5-9.8,4  L256,197.8L124.9,68.3c-2.6-2.6-6.1-4-9.8-4c-3.7,0-7.2,1.5-9.8,4L68,105.9c-5.4,5.4-5.4,14.2,0,19.6l131.5,130L68.4,387.1  c-2.6,2.6-4.1,6.1-4.1,9.8c0,3.7,1.4,7.2,4.1,9.8l37.4,37.6c2.7,2.7,6.2,4.1,9.8,4.1c3.5,0,7.1-1.3,9.8-4.1L256,313.1l130.7,131.1  c2.7,2.7,6.2,4.1,9.8,4.1c3.5,0,7.1-1.3,9.8-4.1l37.4-37.6c2.6-2.6,4.1-6.1,4.1-9.8C447.7,393.2,446.2,389.7,443.6,387.1z" />
                                    </svg>
                                        </div>
                                        <div class="chech_video">
                                            <iframe width="100%" height="100%"
                                                    src="https://www.youtube.com/embed/FkqJ-EnKUxM?list=PLOQDek48BpZFzxKS-2sLAeSBB83x-xILB"
                                                    title="Урок GSAP 3.6 - Начало работы - Эпизод 1" frameborder="0"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                                    allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="home_page_sec_11_p_1">Dave Goldblatt</div>
                            <div class="home_page_sec_11_p_2">CEO of Wavecut</div>
                        </div>
                    </div>
                </div>
                <button class="home_page_sec_11_btn" id="loadMore">See More</button>
            </div>
        </section>
        <section class="home_page_section_12">
            <div class="home_page_sec_12_cont">
                @foreach($contents->get('home', 'section_12') as $section12)
                    <div class="home_page_text_hidden">
                        <p class="home_page_sec_12_h4">{!! $section12->content !!}</p>
                    </div>
                @endforeach
            </div>
            <div class="home_page_sec_12_cont_tablet">
                @foreach($contents->get('home', 'section_12') as $section12)
                    <div class="home_page_text_hidden">
                        <p class="home_page_sec_12_h4">{!! $section12->content !!}</p>
                    </div>
                @endforeach
            </div>
{{--            <div class="home_page_sec_12_cont_mobile">--}}
{{--                @foreach($contents->get('home', 'section_12') as $section12)--}}
{{--                    <div class="home_page_text_hidden">--}}
{{--                        <p class="home_page_sec_12_h4">{!! $section12->content !!}</p>--}}
{{--                    </div>--}}
{{--                @endforeach--}}
{{--            </div>--}}
            <div class="home_page_sec_12_cont_mobile">
                <div class="home_page_text_hidden">
                    <p class="home_page_sec_12_h4">Switch to solar</p>
                </div>
                <div class="home_page_text_hidden">
                    <p class="home_page_sec_12_h4">today and start</p>
                </div>
                <div class="home_page_text_hidden">
                    <p class="home_page_sec_12_h4">saving money on</p>
                </div>
                <div class="home_page_text_hidden">
                    <p class="home_page_sec_12_h4">your energy bill</p>
                </div>
                <div class="home_page_text_hidden">
                    <p class="home_page_sec_12_h4">today! That’s a</p>
                </div>
                <div class="home_page_text_hidden">
                    <p class="home_page_sec_12_h4">investment</p>
                </div>
                <div class="home_page_text_hidden">
                    <div class="home_page_sec_12_h4_display">
                        <p class="home_page_sec_12_h4">and</p>
                        <p class="home_page_dolar">$</p>
                        <p class="home_page_zero">0</p>
                        <p class="home_page_sec_12_h4">energy</p>
                    </div>
                </div>
                <div class="home_page_text_hidden">
                <div class="home_page_sec_12_h4_display">
                    <p class="home_page_sec_12_h4">bill.</p>
                    <p class="home_page_dolar home_page_dolar_color">$</p>
                    <p class="home_page_zero home_page_zero_color">0</p>
                </div>
                </div>
            </div>
            <div class="home_page_sec_12_info">
                <form action="{{ route('front.formSubmit') }}" method="post" id="home_page_form">
                    @csrf
                    <div class="home_page_sec_12_text_cont">
                        <div class="home_page_text_hidden">
                            <p class="home_page_sec_12_h5">Lorem ipsum dolor sit amet consectetur.</p>
                        </div>
                    </div>
                    <div class="home_page_sec_12_text_cont_tablet">
                        <div class="home_page_text_hidden">
                            <p class="home_page_sec_12_h5">Lorem ipsum dolor sit amet</p>
                        </div>
                        <div class="home_page_text_hidden">
                            <p class="home_page_sec_12_h5">consectetur.</p>
                        </div>
                    </div>
                    <div class="home_page_sec_12_text_cont_mobile">
                        <div class="home_page_text_hidden">
                            <p class="home_page_sec_12_h5">Lorem ipsum dolor</p>
                        </div>
                        <div class="home_page_text_hidden">
                            <p class="home_page_sec_12_h5">sit amet</p>
                        </div>
                        <div class="home_page_text_hidden">
                            <p class="home_page_sec_12_h5">consectetur.</p>
                        </div>
                    </div>
                    <input type="hidden" name="subject" value="Lorem ipsum dolor sit amet consectetur.">
                    <input type="hidden" name="type" value="{{ \App\Models\FormSubmission::TYPE_GLOBAL }}">
                    <p class="home_page_sec_12_p">Lorem ipsum dolor sit amet consectetur. Blandit fermentum praesen</p>
                    <div class="home_page_cont_input">
                        <input class="home_page_sec_12_input" type="text" name="full_name" placeholder="Full name">
                    </div>
                    <div class="home_page_cont_input_2">
                        <input class="home_page_sec_12_input" type="text" name="email" placeholder="Email address">
                    </div>
                    <div class="home_page_cont_input_3">
                        <input class="home_page_sec_12_input" type="text" name="phone_number" placeholder="Phone number">
                    </div>
                    <div class="home_page_cont_input_4">
                        <textarea class="home_page_sec_12_input_2" type="text" name="message" placeholder="Message"></textarea>
                    </div>
                    <button class="home_page_sec_12_btn" type="button" id="submit_button">
                        Submit
                        <i class="fas fa-circle-notch fa-spin" id="spinner" style="display: none"></i>
                    </button>
                </form>
            </div>
    </section>
@endsection
@section('scripts')
    <script src="{{ asset('js/swiper-4.4.6.js') }}"></script>
    <script src="{{ asset('js/jquery-1.12.0.js') }}"></script>
    <script src="{{ asset('js/jquery-easing-1.3.js') }}"></script>
    <script src="{{ asset('js/jquery-3.1.1.js') }}"></script>
    <script src="{{ asset('js/gsap-latest-beta.js') }}"></script>
    <script src="{{ asset('js/scroll-trigger.js') }}"></script>
    <script src="{{ asset('js/scrollsmoother.js') }}"></script>
    <script src="{{ asset('js/gsap-3.11.4-ScrollTrigger.js') }}"></script>
    <script src="{{ asset('js/gsap-3.11.4.js') }}"></script>
    <script src="{{ asset('js/front/scriptHomePage.js') }}"></script>
    <script>

        $(document).ready(function () {

            let form = $('#home_page_form')
            $('body').on('click', '#submit_button', function () {
                loading(true)
                let formData = new FormData(form[0])
                axios.post(form.attr('action'), formData).then(({data}) => {
                    validationErrors(form, {})
                    loading(false)
                }).catch(({response}) => {
                    if (response.status === 422) {
                        validationErrors(form, response.data?.errors)
                    }
                    loading(false)
                })
            })

            function loading(statement) {
                let loading = $('#spinner')
                loading.css('display', 'none')
                if (statement) {
                    loading.css('display', 'block')
                }
            }

            function validationErrors(form, errors = {}) {
                if (!Object.keys(errors).length) {
                    form.find('input,textarea,select').map(function (index, $input) {
                        $input = $($input)
                        let errorSpan = $input.next('span')
                        if (errorSpan) {
                            $input.val('')
                            $input.text('')
                            errorSpan.text('')
                            errorSpan.css('display', 'none')
                        }
                    })
                    return true;
                }
                form.find('input,textarea,select').map(function (index, $input) {
                    $input = $($input)
                    let errorSpan = $input.next('span')
                    errorSpan.text('')
                    errorSpan.css('display', 'none')
                    if (errors[$input.attr('name')]) {
                        errorSpan.css('display', 'block')
                        errorSpan.text(errors[$input.attr('name')][0])
                        console.log('input', $input)
                    }
                })
                return true;
            }
        })
    </script>
@endsection
