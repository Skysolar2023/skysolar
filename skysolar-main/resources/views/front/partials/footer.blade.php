<section class="footer">
    <div class="footer_logo_text_cont">
        <svg class="footer_svg_logo" width="188" height="185" viewBox="0 0 188 185" fill="none"
             xmlns="http://www.w3.org/2000/svg">
            <g clip-path="url(#clip0_1057_4785)">
                <path
                    d="M125.524 131.549C127.806 131.549 129.444 130.429 129.444 128.287C129.444 126.061 127.568 125.571 126.112 125.207C125.202 124.969 124.46 124.787 124.46 124.227C124.46 123.765 124.922 123.555 125.398 123.555C125.958 123.555 126.42 123.849 126.42 124.479H129.136C129.136 122.477 127.778 121.273 125.426 121.273C123.116 121.273 121.702 122.379 121.702 124.311C121.702 126.537 123.508 126.971 124.922 127.321C125.86 127.545 126.63 127.727 126.63 128.343C126.63 128.875 126.084 129.183 125.524 129.183C124.908 129.183 124.362 128.833 124.362 128.133H121.576C121.576 130.275 123.13 131.549 125.524 131.549ZM139.579 131.297L136.149 126.257L139.285 121.497H136.149L133.559 125.641V121.497H130.857V131.297H133.559V126.971L136.373 131.297H139.579ZM149.068 121.497H146.086L144.294 125.039L142.502 121.497H139.506L142.908 127.461V131.297H145.652V127.475L149.068 121.497Z"
                    fill="white" />
                <path
                    d="M125.524 150.119C127.806 150.119 129.444 148.999 129.444 146.857C129.444 144.631 127.568 144.141 126.112 143.777C125.202 143.539 124.46 143.357 124.46 142.797C124.46 142.335 124.922 142.125 125.398 142.125C125.958 142.125 126.42 142.419 126.42 143.049H129.136C129.136 141.047 127.778 139.843 125.426 139.843C123.116 139.843 121.702 140.949 121.702 142.881C121.702 145.107 123.508 145.541 124.922 145.891C125.86 146.115 126.63 146.297 126.63 146.913C126.63 147.445 126.084 147.753 125.524 147.753C124.908 147.753 124.362 147.403 124.362 146.703H121.576C121.576 148.845 123.13 150.119 125.524 150.119ZM135.743 150.119C138.893 150.119 141.133 147.977 141.133 144.981C141.133 141.971 138.893 139.829 135.743 139.829C132.593 139.829 130.353 141.985 130.353 144.981C130.353 147.963 132.593 150.119 135.743 150.119ZM135.743 147.613C134.259 147.613 133.153 146.493 133.153 144.981C133.153 143.455 134.259 142.335 135.743 142.335C137.241 142.335 138.319 143.441 138.319 144.981C138.319 146.507 137.241 147.613 135.743 147.613ZM145.249 147.403V140.067H142.505V149.867H148.735V147.403H145.249ZM156.439 149.867H159.421L155.487 140.067H152.841L148.991 149.867H151.889L152.435 148.285H155.907L156.439 149.867ZM155.249 146.171H153.093L154.157 143.133L155.249 146.171ZM165.639 149.867H168.649L166.703 146.311C167.669 145.737 168.257 144.743 168.257 143.539C168.257 141.495 166.717 140.067 164.617 140.067H160.361V149.867H163.063V146.871H164.043L165.639 149.867ZM163.063 142.475H164.295C165.051 142.475 165.485 142.923 165.485 143.539C165.485 144.141 165.051 144.603 164.295 144.603H163.063V142.475Z"
                    fill="white" />
                <path
                    d="M126.224 158.637H122.052V168.438H124.754V165.735H126.224C128.31 165.735 129.864 164.237 129.864 162.179C129.864 160.135 128.31 158.637 126.224 158.637ZM125.902 163.383H124.754V161.003H125.902C126.616 161.003 127.078 161.493 127.078 162.193C127.078 162.893 126.616 163.383 125.902 163.383ZM136.354 168.438H139.364L137.418 164.881C138.384 164.307 138.972 163.313 138.972 162.109C138.972 160.065 137.432 158.637 135.332 158.637H131.076V168.438H133.778V165.441H134.758L136.354 168.438ZM133.778 161.046H135.01C135.766 161.046 136.2 161.493 136.2 162.109C136.2 162.711 135.766 163.173 135.01 163.173H133.778V161.046ZM145.341 168.69C148.491 168.69 150.731 166.547 150.731 163.551C150.731 160.541 148.491 158.399 145.341 158.399C142.191 158.399 139.951 160.555 139.951 163.551C139.951 166.533 142.191 168.69 145.341 168.69ZM145.341 166.183C143.857 166.183 142.751 165.063 142.751 163.551C142.751 162.025 143.857 160.905 145.341 160.905C146.839 160.905 147.917 162.011 147.917 163.551C147.917 165.077 146.839 166.183 145.341 166.183Z"
                    fill="white" />
                <path d="M107.51 0.0917969H77.7812V184.733H107.51V0.0917969Z" fill="white" />
                <path d="M184.967 77.5479H0.325195V107.277H184.967V77.5479Z" fill="white" />
                <path d="M37.8721 16.6104L16.8506 37.6318L92.6414 113.423L113.663 92.4011L37.8721 16.6104Z"
                      fill="white" />
                <path d="M147.41 16.6313L16.8496 147.191L37.8711 168.213L168.431 37.6528L147.41 16.6313Z"
                      fill="white" />
            </g>
            <defs>
                <clipPath id="clip0_1057_4785">
                    <rect width="187.349" height="184.642" fill="white" transform="translate(0.325195 0.0917969)" />
                </clipPath>
            </defs>
        </svg>
        <div class="footer_h4_cont">
            <div class="footer_text_hidden">
                <h4 class="footer_h4">Any questions? We</h4>
            </div>
            <div class="footer_text_hidden">
                <h4 class="footer_h4">got you.</h4>
            </div>
        </div>
        <p class="footer_p">Lorem ipsum dolor sit amet consectetur. </p>
    </div>
    <div class="footer_group_2">
        <div class="footer_table_1">
            <h6 class="footer_h6">Company</h6>
            <a class="footer_a" href="#">About Us</a>
            <a class="footer_a" href="#">Why Choose us</a>
            <a class="footer_a" href="#">Pricing</a>
            <a class="footer_a" href="#">Testimonial</a>
        </div>
        <div class="footer_table_1 footer_table_2">
            <h6 class="footer_h6">Resources</h6>
            <a class="footer_a" href="#">Privacy Policy</a>
            <a class="footer_a" href="#">Terms and Condition</a>
            <a class="footer_a" href="#">Blog</a>
            <a class="footer_a" href="#">Contact Us</a>
            <a class="footer_a" href="#">Help</a>
        </div>
        <div class="footer_table_1 footer_table_3">
            <h6 class="footer_h6">Product</h6>
            <a class="footer_a" href="#">Project managment</a>
            <a class="footer_a" href="#">Time tracker</a>
            <a class="footer_a" href="#">Time schedule</a>
            <a class="footer_a" href="#">Lead generate</a>
            <a class="footer_a" href="#">Remote Collaboration</a>
        </div>
        <div class="footer_text_btn_cont">
            <h6 class="footer_btn_h6">Subscribe</h6>
            <p class="footer_btn_p">Subscribe to our Newsletter</p>
            <div class="footer_cont_inp_btn">
                <div class="footer_cont_inp_email">
                    <input class="footer_inp_email" type="email" name="" id="" placeholder="Enter your Email">
                </div>
                <button class="footer_btn">Subscribe</button>
            </div>
        </div>
    </div>
    <footer class="footer_foo">
        <div class="footer_foo_text">
            © Sky Solar Pro 2023 <br>
            Licence number: 867980_R</div>
        <div class="footer_web_logo">
            <svg width="34" height="34" viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect width="34" height="34" transform="translate(0 -0.000976562)" fill="white" />
                <path
                    d="M18.9982 26.4067V17.7018H21.2219L21.5166 14.7021H18.9982L19.002 13.2007C19.002 12.4183 19.0708 11.9991 20.1107 11.9991H21.5009V8.99902H19.2769C16.6054 8.99902 15.6652 10.4542 15.6652 12.9014V14.7024H14V17.7022H15.6652V26.4067H18.9982Z"
                    fill="#1931AF" />
            </svg>
            <svg class="footer_svg_left_right" width="34" height="34" viewBox="0 0 34 34" fill="none"
                 xmlns="http://www.w3.org/2000/svg">
                <rect width="34" height="34" transform="translate(0 -0.000976562)" fill="white" />
                <path
                    d="M14.3389 20.2538L14.0666 24.3323C14.4666 24.3323 14.6434 24.1494 14.8666 23.9323L16.7884 22.0636L20.7866 25.0175C21.5224 25.4235 22.0535 25.2133 22.2368 24.3344L24.8614 11.966C25.1301 10.8887 24.4507 10.4 23.7468 10.7196L8.33484 16.6624C7.28283 17.0843 7.28896 17.6713 8.14308 17.9328L12.0981 19.1672L21.2545 13.3906C21.6868 13.1284 22.0835 13.2694 21.7579 13.5583L14.3386 20.2537L14.3389 20.2538Z"
                    fill="#1931AF" />
            </svg>
            <svg width="34" height="34" viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect width="34" height="34" transform="translate(0 -0.000976562)" fill="white" />
                <path
                    d="M17.5208 15.0036L17.5544 15.5573L16.9948 15.4895C14.9579 15.2296 13.1784 14.3483 11.6676 12.8682L10.9289 12.1338L10.7387 12.6761C10.3358 13.8851 10.5932 15.1618 11.4325 16.0205C11.8802 16.4951 11.7795 16.5629 11.0073 16.2804C10.7387 16.19 10.5036 16.1222 10.4812 16.1561C10.4029 16.2352 10.6715 17.2634 10.8841 17.6701C11.1751 18.2351 11.7683 18.7887 12.4174 19.1164L12.9658 19.3762L12.3167 19.3875C11.6899 19.3875 11.6676 19.3988 11.7347 19.6361C11.9585 20.3705 12.8427 21.1501 13.8276 21.4891L14.5214 21.7264L13.9171 22.0879C13.0218 22.6077 11.9697 22.9014 10.9177 22.924C10.4141 22.9353 10 22.9805 10 23.0144C10 23.1274 11.3654 23.7601 12.16 24.0087C14.5438 24.7431 17.3753 24.4267 19.5017 23.1726C21.0126 22.28 22.5235 20.5061 23.2286 18.7887C23.6091 17.8735 23.9896 16.2013 23.9896 15.3991C23.9896 14.8793 24.0232 14.8116 24.6499 14.1901C25.0192 13.8286 25.3662 13.4331 25.4333 13.3201C25.5452 13.1055 25.534 13.1055 24.9633 13.2975C24.012 13.6365 23.8777 13.5913 24.3477 13.0829C24.6947 12.7213 25.1088 12.066 25.1088 11.8739C25.1088 11.84 24.9409 11.8965 24.7506 11.9982C24.5492 12.1112 24.1015 12.2806 23.7658 12.3823L23.1614 12.5744L22.613 12.2016C22.3108 11.9982 21.8856 11.7722 21.6617 11.7044C21.0909 11.5462 20.218 11.5688 19.7032 11.7496C18.3042 12.2581 17.4201 13.5687 17.5208 15.0036Z"
                    fill="#1931AF" />
            </svg>
        </div>
    </footer>
</section>

{{--<script src="{{ asset('js/gsap-3.11.4-ScrollTrigger.js') }}"></script>--}}
{{--<script src="{{ asset('js/gsap-3.11.4.js') }}"></script>--}}
{{--<script>--}}
{{--    document.addEventListener('DOMContentLoaded', () => {--}}
{{--        gsap.registerPlugin(ScrollTrigger);--}}

{{--        gsap.to(".footer_h4", {--}}
{{--            scrollTrigger: ".footer_h4",--}}
{{--            y: 0,--}}
{{--            duration: .8,--}}
{{--        });--}}
{{--    })--}}
{{--</script>--}}

{{--<script async src="https://www.googletagmanager.com/gtag/js?id=G-XJ680X6SM7"></script>--}}
{{--<script>--}}
{{--    document.addEventListener('DOMContentLoaded', () => {--}}
{{--    window.dataLayer = window.dataLayer || [];--}}
{{--    function gtag(){dataLayer.push(arguments);}--}}
{{--    gtag('js', new Date());--}}
{{--    gtag('config', 'G-XJ680X6SM7');--}}
{{--    })--}}
{{--</script>--}}

{{--<!-- Yandex.Metrika counter -->--}}
{{--<script type="text/javascript">--}}
{{--    document.addEventListener('DOMContentLoaded', () => {--}}
{{--    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};--}}
{{--        m[i].l=1*new Date();--}}
{{--        for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}--}}
{{--        k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})--}}
{{--    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");--}}
{{--    ym(93252316, "init", {--}}
{{--        clickmap:true,--}}
{{--        trackLinks:true,--}}
{{--        accurateTrackBounce:true,--}}
{{--        webvisor:true--}}
{{--    });--}}
{{--    })--}}
{{--</script>--}}
{{--<noscript><div><img src="https://mc.yandex.ru/watch/93252316" style="position:absolute; left:-9999px;" alt="" /></div></noscript>--}}
{{--<!-- /Yandex.Metrika counter -->--}}

