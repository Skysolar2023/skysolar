@extends('layouts.front')

@section('content')
    <section class="blog-section-1">
        <div class="blog-sec-1-text-cont-pc">
            <div class="blog-text-hidden">
                <h3 class="blog-sec-1-h3">Latest market trends,</h3>
            </div>
            <div class="blog-text-hidden">
                <h3 class="blog-sec-1-h3">know-hows, tips, and news</h3>
            </div>
            <div class="blog-text-hidden">
                <h3 class="blog-sec-1-h3">all in our blog.</h3>
            </div>
        </div>
        <div class="blog-sec-1-text-cont-mobile">
            <div class="blog-text-hidden">
                <h3 class="blog-sec-1-h3">Latest market</h3>
            </div>
            <div class="blog-text-hidden">
                <h3 class="blog-sec-1-h3">trends,know-</h3>
            </div>
            <div class="blog-text-hidden">
                <h3 class="blog-sec-1-h3">hows, tips,</h3>
            </div>
            <div class="blog-text-hidden">
                <h3 class="blog-sec-1-h3">and news all</h3>
            </div>
            <div class="blog-text-hidden">
                <h3 class="blog-sec-1-h3">in our blog.</h3>
            </div>
        </div>
        <div class="blog-category-cont">
            <a href="/blog?category=all" class="blog-category-p">All</a>
            @foreach($categories as $category)
                <a class="blog-category-p" href="/blog?category={{ $category->slug }}">{{$category->name}}</a>
            @endforeach
        </div>

{{--        <div class="blog-swiper-container">--}}
{{--            <div class="blog-swiper-wrapper">--}}
{{--                <div class="blog-swiper-slide">--}}
{{--                    <div class="blog-swiper-slide-img-cont">--}}
{{--                        <p class="blog-category-p blog-category-width">All</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="blog-swiper-slide">--}}
{{--                    <div class="blog-swiper-slide-img-cont">--}}
{{--                        <p class="blog-category-p blog-category-width-2">Category 1</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="blog-swiper-slide">--}}
{{--                    <div class="blog-swiper-slide-img-cont">--}}
{{--                        <p class="blog-category-p blog-category-width-2">Category 2</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="blog-swiper-slide">--}}
{{--                    <div class="blog-swiper-slide-img-cont">--}}
{{--                        <p class="blog-category-p blog-category-width-2">Category 3</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="blog-swiper-slide">--}}
{{--                    <div class="blog-swiper-slide-img-cont">--}}
{{--                        <p class="blog-category-p blog-category-width-2">Category 4</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="blog-text-hidden">
            <div class="blog-sec-1-h4">Our blog.dolor sit</div>
        </div>
        <div class="blog-text-hidden">
            <div class="blog-sec-1-h4">lorem ipsum</div>
        </div>
        <div class="blog-items-wrapper">
            <div id="on-items-bucket"></div>
            <div id="off-items-bucket">
                <div class="blog-item">
                    <div class="blog-cont">
                        @foreach($blogList as $blog)
                            <div class="blog-info">
                                <a class="blog-a" href="{{ route('blog.single', $blog->slug) }}" target="_blank">
                                    <img class="blog-sec-1-img" src="{{ $blog->mainImage->urls['medium'] }}" alt="{{ $blog->mainImage->alt }}">
                                    <p class="blog-info-p-1">{{ $blog->title }}</p>
                                    <p class="blog-info-p-2">{{ $blog->description }}</p>
                                </a>
                            </div>
                        @endforeach
                    </div>
{{--                    <div class="blog-cont blog-cont-top">--}}
{{--                        <div class="blog-info">--}}
{{--                            <a class="blog-a" href="BlogSinglePage.html" target="_blank">--}}
{{--                                <img class="blog-sec-1-img" src="{{ asset('images/static/Rectangle 12(3).webp') }}" alt="">--}}
{{--                                <p class="blog-info-p-1">Solar pannel</p>--}}
{{--                                <p class="blog-info-p-2">Lorem ipsum dolor sit amet consectetur. Blandit fermentum--}}
{{--                                    praesen</p>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="blog-info">--}}
{{--                            <a class="blog-a" href="BlogSinglePage.html" target="_blank">--}}
{{--                                <img class="blog-sec-1-img" src="{{ asset('images/static/Rectangle 12(4).webp') }}" alt="">--}}
{{--                                <p class="blog-info-p-1">Solar pannel</p>--}}
{{--                                <p class="blog-info-p-2">Lorem ipsum dolor sit amet consectetur. Blandit fermentum--}}
{{--                                    praesen</p>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
{{--                <div class="blog-item">--}}
{{--                    <div class="blog-cont blog-cont-top">--}}
{{--                        <div class="blog-info">--}}
{{--                            <a class="blog-a" href="BlogSinglePage.html" target="_blank">--}}
{{--                                <img class="blog-sec-1-img" src="{{ asset('images/static/Rectangle 12(3).webp') }}" alt="">--}}
{{--                                <p class="blog-info-p-1">Solar pannel</p>--}}
{{--                                <p class="blog-info-p-2">Lorem ipsum dolor sit amet consectetur. Blandit fermentum--}}
{{--                                    praesen</p>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="blog-info">--}}
{{--                            <a class="blog-a" href="BlogSinglePage.html" target="_blank">--}}
{{--                                <img class="blog-sec-1-img" src="{{ asset('images/static/Rectangle 12(4).webp') }}" alt="">--}}
{{--                                <p class="blog-info-p-1">Solar pannel</p>--}}
{{--                                <p class="blog-info-p-2">Lorem ipsum dolor sit amet consectetur. Blandit fermentum--}}
{{--                                    praesen</p>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
            {{ $blogList->links() }}
            <button class="blog-sec-1-btn" id="load-more">Load More</button>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{ asset('js/jquery-1.12.0.js') }}"></script>
    <script src="{{ asset('js/blog-swiper-4.3.0.js') }}"></script>
    <script src="{{ asset('js/gsap-3.11.4-ScrollTrigger.js') }}"></script>
    <script src="{{ asset('js/gsap-3.11.4.js') }}"></script>
    <script src="{{ asset('js/front/blog.js') }}"></script>
@endsection
