@extends('layouts.front')

@section('content')
    <section class="roof-section-1">
        <div class="roof-sec-1-text-cont">
            <div class="roof-text-hidden">
                <h3 class="roof-sec-1-h3">OUR VISION FOR CUSTOMERS:</h3>
            </div>
            <div class="roof-text-hidden">
                <div class="roof-sec-1-text-img-cont">
                    <h3 class="roof-sec-1-h3">PROTECT YOUR HOME AND</h3>
                    <img class="roof-sec-1-img" src="{{ asset('images/static/solar panel2.webp') }}" alt="">
                </div>
            </div>
            <div class="roof-text-hidden">
                <h3 class="roof-sec-1-h3">OPTIMIZE YOUR ENERGY USAGE</h3>
            </div>
            <div class="roof-text-hidden">
                <div class="roof-texts-cont">
                    <div class="roof-text-hidden">
                        <h3 class="roof-sec-1-h3">WITH A NEW ROOF</h3>
                    </div>
                    <p class="roof-sec-1-p">Your roof is there to protect your home. It keeps the house dry and comfortable
                        all
                        year round and protects your interior from harsh weather.</p>
                    <p class="roof-sec-1-p-mobile">Since 2008, Solar Optimum has paved the way for premium solar, battery
                        storage and roofing solutions for residential and commercial customers in California.</p>
                    <img class="roof-sec-1-img-tablet" src="{{ asset('images/static/solar panel2.webp') }}" alt="">
                </div>
            </div>
        </div>
        <div class="roof-sec-1-text-cont-mobile">
            <div class="roof-text-hidden">
                <h3 class="roof-sec-1-h3">OUR VISION FOR </h3>
            </div>
            <div class="roof-text-hidden">
                <h3 class="roof-sec-1-h3">CUSTOMERS:</h3>
            </div>
            <div class="roof-text-hidden">
                <div class="roof-text-1-img-text">
                    <img class="roof-sec-1-img-mobile" src="{{ asset('images/static/solar panel2.webp') }}" alt="">
                    <h3 class="roof-sec-1-h3">TO BECOME</h3>
                </div>
            </div>
            <div class="roof-text-hidden">
                <h3 class="roof-sec-1-h3">100% ENERGY</h3>
            </div>
            <div class="roof-text-hidden">
                <h3 class="roof-sec-1-h3">INDEPENDENT</h3>
            </div>
            <p class="roof-sec-1-p-mobile">Since 2008, Solar Optimum has paved the way for premium solar, battery
                storage and roofing solutions for residential and commercial customers in California.</p>
        </div>
    </section>
    <section class="roof-section-2">
        <div class="roof-sec-2-img-text-cont">
            <div class="roof-text-cont">
                <div class="roof-text-hidden">
                    <h3 class="roof-sec-2-h3">The leader in solar</h3>
                </div>
                <div class="roof-text-hidden">
                    <h3 class="roof-sec-2-h3">and roofing.</h3>
                </div>
                <p class="roof-sec-2-p-1">Solar Optimum has been a leading solar installer since 2008. We’ve earned the
                    trust, respect, and confidence of homeowners across the state thanks to our dedicated and talented
                    team of experts. </p>
                <p class="roof-sec-2-p-2">We are proud to have expanded our offerings to include residential roofing.</p>
            </div>
            <div class="roof-img-cont">
                <img class="roof-sec-2-img" src="{{ asset('images/static/Rectangle 21(2).webp') }}" alt="">
            </div>
        </div>
        <div class="roof-sec-2-yellow-cont">
            <div class="roof-yellow-text-cont">
                <div class="roof-yellow-text-cont-pc">
                    <div class="roof-text-hidden">
                        <h4 class="roof-yellow-h4">Now Is The Time To</h4>
                    </div>
                    <div class="roof-text-hidden">
                        <h4 class="roof-yellow-h4">Contact Sky Solar Pro.</h4>
                    </div>
                </div>
                <div class="roof-yellow-text-cont-tablet">
                    <div class="roof-text-hidden">
                        <h4 class="roof-yellow-h4-2">Now Is The Time</h4>
                    </div>
                    <div class="roof-text-hidden">
                        <h4 class="roof-yellow-h4-2">To Contact Sky</h4>
                    </div>
                    <div class="roof-text-hidden">
                        <h4 class="roof-yellow-h4-2">Solar Pro.</h4>
                    </div>
                </div>
                <p class="roof-yellow-p">Solar Optimum Is Changing The Way California, Nevada, and Arizona Homeowners
                    Conserve Energy.</p>
                <button class="roof-yellow-btn">Get a Quote</button>
            </div>
        </div>
    </section>
    <section class="roof-section-3">
        <div class="roof-text-hidden">
            <h3 class="roof-sec-3-h3">A GAF MASTER ELITE</h3>
        </div>
        <div class="roof-text-hidden">
            <div class="roof-text-h3-p-cont">
                <div class="roof-text-hidden">
                    <h3 class="roof-sec-3-h3">CONTRACTOR</h3>
                </div>
                <p class="roof-sec-3-p">We are proud to be a GAF Master Elite Contractor for residential roofing – an honor
                    only shared by 2 percent of roofing contractors in the entire United States.</p>
            </div>
        </div>
        <img class="roof-sec-3-img" src="{{ asset('images/static/Rectangle 12 (1)12.webp') }}" alt="">
        <div class="roof-item">
            <div class="roof-column">
                <div id="counter-box">
                    <span class="roof-counter" data-number="57"></span>
                    <p class="roof-plyus">%</p>
                </div>
                <p class="roof-item-p">Lorem ipsum dolor sit</p>
            </div>
            <div class="roof-column">
                <div id="counter-box">
                    <span class="roof-counter" data-number="174"></span>
                    <p class="roof-plyus">+</p>
                </div>
                <p class="roof-item-p">Lorem ipsum dolor sit</p>
            </div>
            <div class="roof-column">
                <div id="counter-box">
                    <span class="roof-counter" data-number="463"></span>
                </div>
                <p class="roof-item-p">Lorem ipsum dolor sit</p>
            </div>
        </div>
    </section>
    <section class="roof-section-4">
        <div class="roof-sec-4-text-cont">
            <div class="roof-sec-4-text-cont-pc">
                <div class="roof-text-hidden">
                    <h3 class="roof-sec-4-h3">Get Our Optimum</h3>
                </div>
                <div class="roof-text-hidden">
                    <h3 class="roof-sec-4-h3">Guide To Buying A</h3>
                </div>
                <div class="roof-text-hidden">
                    <h3 class="roof-sec-4-h3">New Roof.</h3>
                </div>
            </div>
            <div class="roof-sec-4-text-cont-tablet">
                <div class="roof-text-hidden">
                    <h3 class="roof-sec-4-h3">Get Our Optimum Guide</h3>
                </div>
                <div class="roof-text-hidden">
                    <h3 class="roof-sec-4-h3">To Buying A New Roof.</h3>
                </div>
            </div>
            <div class="roof-sec-4-text-cont-mobile">
                <div class="roof-text-hidden">
                    <h3 class="roof-sec-4-h3-2">Get Our</h3>
                </div>
                <div class="roof-text-hidden">
                    <h3 class="roof-sec-4-h3-2">Optimum Guide</h3>
                </div>
                <div class="roof-text-hidden">
                    <h3 class="roof-sec-4-h3-2">To Buying A</h3>
                </div>
                <div class="roof-text-hidden">
                    <h3 class="roof-sec-4-h3-2">New Roof.</h3>
                </div>
            </div>
            <p class="roof-sec-4-p">Download this FREE guide that will help you consider the most important factors that
                impact your decision for a new roof!</p>
            <div class="roof-svg-text-cont">
                <div class="roof-cont">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M20 6L9 17L4 12" stroke="black" stroke-width="2" stroke-linecap="round"
                              stroke-linejoin="round" />
                    </svg>
                    <p class="roof-cont-p">Contractor's years of experience</p>
                </div>
                <div class="roof-cont">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M20 6L9 17L4 12" stroke="black" stroke-width="2" stroke-linecap="round"
                              stroke-linejoin="round" />
                    </svg>
                    <p class="roof-cont-p">Contractor's years of experience</p>
                </div>
                <div class="roof-cont">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M20 6L9 17L4 12" stroke="black" stroke-width="2" stroke-linecap="round"
                              stroke-linejoin="round" />
                    </svg>
                    <p class="roof-cont-p">Contractor's years of experience</p>
                </div>
                <div class="roof-cont">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M20 6L9 17L4 12" stroke="black" stroke-width="2" stroke-linecap="round"
                              stroke-linejoin="round" />
                    </svg>
                    <p class="roof-cont-p">Contractor's years of experience</p>
                </div>
                <div class="roof-cont">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M20 6L9 17L4 12" stroke="black" stroke-width="2" stroke-linecap="round"
                              stroke-linejoin="round" />
                    </svg>
                    <p class="roof-cont-p">Contractor's years of experience</p>
                </div>
            </div>
            <button class="roof-sec-4-btn">Download Guide
                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path id="svg-color"
                          d="M21.5 15.5V19.5C21.5 20.0304 21.2893 20.5391 20.9142 20.9142C20.5391 21.2893 20.0304 21.5 19.5 21.5H5.5C4.96957 21.5 4.46086 21.2893 4.08579 20.9142C3.71071 20.5391 3.5 20.0304 3.5 19.5V15.5"
                          stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                    <path id="svg-color" d="M7.5 10.5L12.5 15.5L17.5 10.5" stroke="white" stroke-width="2"
                          stroke-linecap="round" stroke-linejoin="round" />
                    <path id="svg-color" d="M12.5 15.5V3.5" stroke="white" stroke-width="2" stroke-linecap="round"
                          stroke-linejoin="round" />
                </svg>
            </button>
        </div>
        <div class="roof-sec-4-img-cont">
            <img class="roof-sec-4-img" src="{{ asset('images/static/Frame 427318530.webp') }}" alt="">
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{ asset('js/jquery-3.5.1.js') }}"></script>
    <script src="{{ asset('js/gsap-3.11.4-ScrollTrigger.js') }}"></script>
    <script src="{{ asset('js/gsap-3.11.4.js') }}"></script>
    <script src="{{ asset('js/front/roof.js') }}"></script>
@endsection
