@extends('layouts.front')

@section('content')
    <section class="examples-section-1">
        <div class="examples-sec-1-text-cont">
            <div class="examples-text-hidden">
                <h3 class="examples-sec-1-h3">Power Up Your Life with</h3>
            </div>
            <div class="examples-text-hidden">
                <h3 class="examples-sec-1-h3">Solar Panels: Explore Our</h3>
            </div>
            <div class="examples-text-hidden">
                <h3 class="examples-sec-1-h3">Gallery Today!</h3>
            </div>
        </div>
        <div class="examples-sec-1-text-mobile">
            <div class="examples-text-hidden">
                <h3 class="examples-sec-1-h3">Power Up Your</h3>
            </div>
            <div class="examples-text-hidden">
                <h3 class="examples-sec-1-h3">Life with Solar</h3>
            </div>
            <div class="examples-text-hidden">
                <h3 class="examples-sec-1-h3">Panels:</h3>
            </div>
            <div class="examples-text-hidden">
                <h3 class="examples-sec-1-h3">Explore Our</h3>
            </div>
            <div class="examples-text-hidden">
                <h3 class="examples-sec-1-h3">Gallery Today!</h3>
            </div>
        </div>
        <div class="examples-category-cont">
            <a href="/examples?category=all" class="examples-category-p">All</a>
            @foreach($categories as $category)
                <a href="/examples?category={{ $category->slug }}" class="examples-category-p">{{ $category->name }}</a>
            @endforeach
        </div>
        <div class="examples-swiper-container">
            <div class="examples-swiper-wrapper">
                <div class="examples-swiper-slide">
                    <div class="examples-swiper-slide-img-cont">
                        <a href="/examples?category=all" class="examples-category-p examples-category-width">All</a>
                    </div>
                </div>
                @foreach($categories as $category)
                    <div class="examples-swiper-slide">
                        <div class="examples-swiper-slide-img-cont">
                            <a href="/examples?category={{ $category->slug }}" class="examples-category-p examples-category-width-2">{{ $category->name }}</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="examples-items-wrapper">
            <div id="on-items-bucket"></div>
            <div id="off-items-bucket">
                <div class="examples-item">
                    <div class="examples-blog-cont">
                        @foreach($examples as $example)
                            <div class="examples-blog-info">
                                <a class="examples-blog-a" href="{{ route('examples-single', $example->slug) }}" target="_blank">
                                    <img class="examples-sec-1-img" src="{{ $example->mainImage->urls['medium'] }}" alt="">
                                    <p class="examples-blog-info-p-1">{{ $example->title }}</p>
                                    <p class="examples-blog-info-p-2">{{ $example->description }}</p>
                                </a>
                            </div>
                        @endforeach
                    </div>
{{--                    <div class="examples-blog-cont examples-blog-cont-top">--}}
{{--                        <div class="examples-blog-info">--}}
{{--                            <a class="examples-blog-a" href="BlogSinglePage.html" target="_blank">--}}
{{--                                <img class="examples-sec-1-img" src="{{ asset('images/static/Rectangle 12(3).webp') }}" alt="">--}}
{{--                                <p class="examples-blog-info-p-1">Solar pannel</p>--}}
{{--                                <p class="examples-blog-info-p-2">Project Name - 196,710 kWh</p>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="examples-blog-info">--}}
{{--                            <a class="examples-blog-a" href="BlogSinglePage.html" target="_blank">--}}
{{--                                <img class="examples-sec-1-img" src="{{ asset('images/static/Rectangle 12(4).webp') }}" alt="">--}}
{{--                                <p class="examples-blog-info-p-1">Solar pannel</p>--}}
{{--                                <p class="examples-blog-info-p-2">Project Name - 196,710 kWh</p>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
{{--                <div class="examples-item">--}}
{{--                    <div class="examples-blog-cont examples-blog-cont-top">--}}
{{--                        <div class="examples-blog-info">--}}
{{--                            <a class="examples-blog-a" href="BlogSinglePage.html" target="_blank">--}}
{{--                                <img class="examples-sec-1-img" src="{{ asset('images/static/Rectangle 12(3).webp') }}" alt="">--}}
{{--                                <p class="examples-blog-info-p-1">Solar pannel</p>--}}
{{--                                <p class="examples-blog-info-p-2">Project Name - 196,710 kWh</p>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="examples-blog-info">--}}
{{--                            <a class="examples-blog-a" href="BlogSinglePage.html" target="_blank">--}}
{{--                                <img class="examples-sec-1-img" src="{{ asset('images/static/Rectangle 12(4).webp') }}" alt="">--}}
{{--                                <p class="examples-blog-info-p-1">Solar pannel</p>--}}
{{--                                <p class="examples-blog-info-p-2">Project Name - 196,710 kWh</p>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
            <button class="examples-sec-1-btn" id="load-more">Load More</button>
        </div>
    </section>

@endsection
@section('scripts')
    <script src="{{ asset('js/jquery-1.12.0.js') }}"></script>
    <script src="{{ asset('js/examples-swiper-4.3.0.js') }}"></script>
    <script src="{{ asset('js/gsap-3.11.4-ScrollTrigger.js') }}"></script>
    <script src="{{ asset('js/gsap-3.11.4.js') }}"></script>
    <script src="{{ asset('js/front/example.js') }}"></script>
@endsection
