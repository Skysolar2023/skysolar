@extends('layouts.front')

@section('content')
    <section class="section-1">
        <div class="sec-1-img-cont">
            <img class="sec-1-img" src="{{ $example->mainImage->urls['large'] }}" alt="">
        </div>
        <div class="sec-1-text-cont">
            <div class="text-hidden">
                <h3 class="sec-1-h3">{{ $example->title }}</h3>
                <h3 class="sec-1-h3"></h3>
            </div>
        </div>
    </section>


    <section class="section-2">
        <div class="yellow-sec">
            <div class="yellow-text-cont">
                <div class="text-hidden">
                    <h3 class="sec-2-h3">Project name</h3>
                </div>
                <p class="sec-2-p">Technical Description</p>
                <div class="texts">
                    <div class="blue-text-cont">
                        <p class="blue-text">{{ $example->info['power']}}</p>
                        <p class="min-text">POWER</p>
                    </div>
                    <div class="blue-text-cont">
                        <p class="blue-text">{{ $example->info['annual_productivity']}}</p>
                        <p class="min-text">ANNUAL PRODUCTIVITY</p>
                    </div>
                    <div class="blue-text-cont">
                        <p class="blue-text">{{ $example->info['saving_amount']}}</p>
                        <p class="min-text">SAVING AMOUNT</p>
                    </div>
                </div>
            </div>
            <div class="min-text-cont">
                <div class="text-blue-cont">
                    <p class="text-1">Location:</p>
                    <p class="text-1 text-blue">Los Angeles</p>
                </div>
                <p class="text-1">300 LA Solar 415 1x Solis 3P 80K, 1x Solis 3P 40</p>
            </div>
        </div>
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d12193.349134208018!2d44.51997215!3d40.179306749999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sam!4v1681113787200!5m2!1sru!2sam" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>
    </section>

    <section class="section-3">
        <div class="text-hidden">
            <h3 class="sec-3-h3">Other photos</h3>
        </div>


        <div class="items-wrapper">
            <div id="on-items-bucket"></div>
            <div id="off-items-bucket">
                <div class=" item">
                    <div class="blog-cont">
                        @foreach($example->attachments as $attachment)
                            <div class="blog-info">
                                <a class="blog-a" href="#" target="_blank">
                                    <img class="sec-3-img" src="{{ $attachment->urls['medium'] }}" alt="{{ $attachment->alt }}">
                                    <p class="blog-info-p-1">{{ $attachment->title }}</p>
                                    <p class="blog-info-p-2">{{ $attachment->alt }}</p>
                                </a>
                            </div>
                        @endforeach
                    </div>
{{--                    <div class="blog-cont blog-cont-top">--}}
{{--                        <div class="blog-info">--}}
{{--                            <a class="blog-a" href="BlogSinglePage.html" target="_blank">--}}
{{--                                <img class="sec-3-img" src="images/Rectangle 12(3).webp" alt="">--}}
{{--                                <p class="blog-info-p-1">Solar pannel</p>--}}
{{--                                <p class="blog-info-p-2">Project Name - 196,710 kWh</p>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="blog-info">--}}
{{--                            <a class="blog-a" href="BlogSinglePage.html" target="_blank">--}}
{{--                                <img class="sec-3-img" src="images/Rectangle 12(4).webp" alt="">--}}
{{--                                <p class="blog-info-p-1">Solar pannel</p>--}}
{{--                                <p class="blog-info-p-2">Project Name - 196,710 kWh</p>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
{{--                <div class="item">--}}
{{--                    <div class="blog-cont blog-cont-top">--}}
{{--                        <div class="blog-info">--}}
{{--                            <a class="blog-a" href="BlogSinglePage.html" target="_blank">--}}
{{--                                <img class="sec-3-img" src="images/Rectangle 12(3).webp" alt="">--}}
{{--                                <p class="blog-info-p-1">Solar pannel</p>--}}
{{--                                <p class="blog-info-p-2">Project Name - 196,710 kWh</p>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="blog-info">--}}
{{--                            <a class="blog-a" href="BlogSinglePage.html" target="_blank">--}}
{{--                                <img class="sec-3-img" src="images/Rectangle 12(4).webp" alt="">--}}
{{--                                <p class="blog-info-p-1">Solar pannel</p>--}}
{{--                                <p class="blog-info-p-2">Project Name - 196,710 kWh</p>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
            <button class="sec-1-btn" id="load-more">Load More</button>
        </div>

    </section>
@endsection
@section('scripts')
    <script src="{{ asset('js/jquery-1.12.0.js') }}"></script>
    <script src="{{ asset('js/gsap-3.11.4-ScrollTrigger.js') }}"></script>
    <script src="{{ asset('js/gsap-3.11.4.js') }}"></script>
    <script src="{{ asset('js/front/examples-single-page.js') }}"></script>
@endsection
