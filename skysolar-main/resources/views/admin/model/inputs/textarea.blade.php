<div class="card card-default collapsed-card main-card {{ $classes ?? '' }}">
    <div class="card-header" data-card-widget="collapse">{{ $label ?? ucfirst(str_replace('_', ' ', $field)) }}</div>
    <div class="card-body tinymce-box">
        <textarea class="form-control @error($field) is-invalid @enderror {{ @$tinymce === false ? '' : 'tinymce' }}"
                  name="{{ $field }}"
                  rows="{{ $rows ?? 20 }}"
                  {{ @$disabled ? 'disabled' : '' }}
        >
            {{ old($field, @$entity->$field) }}
        </textarea>
        @error($field)<span class="invalid-feedback">{{ $message }}</span>@enderror
    </div>
</div>
