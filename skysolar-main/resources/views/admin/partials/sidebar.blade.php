<aside class="main-sidebar-admin">
    <!-- Sidebar -->
    <div class="sidebar-admin">
        <!-- Sidebar Menu -->
        <nav>
            <ul class="nav nav-pills nav-sidebar flex-column pt-4" data-widget="treeview" role="menu" data-accordion="false">
                @can('Manage Pages')
                    <li class="nav-item menu">
                        <a href="{{route('attachments.index')}}" class="nav-link">
                            <i class="fas fa-images"></i>
                            <span>Media</span>
                        </a>
                    </li>
                @endcan
                @can('Manage Pages')
                    <li class="nav-item menu">
                        <a href="{{route('page-contents.index')}}" class="nav-link">
                            <i class="fas fa-images"></i>
                            <span>Page Contents</span>
                        </a>
                    </li>
                @endcan
                @can('Manage Pages')
                    <li class="nav-item menu">
                        <a href="{{route('form-submissions.index')}}" class="nav-link">
                            <i class="fas fa-images"></i>
                            <span>Form Submissions</span>
                        </a>
                    </li>
                @endcan
                @can('Manage Pages')
                    <li class="nav-item menu">
                        <a href="{{route('blog.index')}}" class="nav-link">
                            <i class="fas fa-images"></i>
                            <span>Blog</span>
                        </a>
                    </li>
                    <li class="nav-item menu">
                        <a href="{{route('categories.index')}}" class="nav-link">
                            <i class="fas fa-images"></i>
                            <span>Categories</span>
                        </a>
                    </li>
                @endcan
                @can('Manage Pages')
                    <li class="nav-item menu">
                        <a href="{{route('examples.index')}}" class="nav-link">
                            <i class="fas fa-images"></i>
                            <span>Example</span>
                        </a>
                    </li>
                    <li class="nav-item menu">
                        <a href="{{route('categories-example.index')}}" class="nav-link">
                            <i class="fas fa-images"></i>
                            <span>Categories Example</span>
                        </a>
                    </li>
                @endcan
                @can('Manage Pages')
                    <li class="nav-item menu">
                        <a href="{{route('settings.general')}}" class="nav-link"><i
                                class="nav-icon fas fa-sliders-h"></i><span>Settings</span></a>
                        <div class="subMenu">
                            <a href="{{route('settings.general')}}" class="nav-link"><span>General</span></a>
                        </div>
                    </li>
                @endcan
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
