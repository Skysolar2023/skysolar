@extends('layouts.admin')
@section('title', 'Form Submissions')
@section('content')
    <div class="content-wrapper products-content-wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header mb-4">
                                <h1 class="card-title">Form Submissions</h1>
                            </div>
                            <div class="card-body">
                                <div class="card-header mb-4">
                                    <div class="card-tools">
{{--                                        @include('admin.form-submissions.list-actions')--}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 table-form-submissions" id="table-init"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
    @include('admin.form-submissions.list')
@endsection
