<!-- left column -->
<div class="col-md-9 main-content--large">
    <div class="row">
        <div class="col-12 card card-default main-card">
            <div class="card-body">
                @if(@$entity->full_name)
                    @include('admin.model.inputs.text', ['entity' => $entity ?? null, 'field' => 'full_name', 'classes' => 'generate-slug', 'disabled' => true])
                @endif
                @if(@$entity->email)
                    @include('admin.model.inputs.text', ['entity' => $entity ?? null, 'field' => 'email', 'classes' => 'generate-slug', 'disabled' => true])
                @endif
                @if(@$entity->phone_number)
                    @include('admin.model.inputs.text', ['entity' => $entity ?? null, 'field' => 'phone_number', 'classes' => 'generate-slug', 'disabled' => true])
                @endif
                @if(@$entity->city)
                    @include('admin.model.inputs.text', ['entity' => $entity ?? null, 'field' => 'city', 'classes' => 'generate-slug', 'disabled' => true])
                @endif
                @if(@$entity->type)
                    @include('admin.model.inputs.text', ['entity' => $entity ?? null, 'field' => 'type', 'classes' => 'generate-slug', 'disabled' => true])
                @endif
                @if(@$entity->message)
                    @include('admin.model.inputs.textarea', ['entity' => $entity ?? null, 'field' => 'message', 'classes' => 'generate-slug', 'disabled' => true, 'tinymce' => false, 'rows' => 5])
                @endif
                @if(@$entity->attachment)
                    <div class="form-group row pl-3 pr-3">
                        <a class="btn btn-primary" href="{{ route('admin.form-submissions.downloadCv', $entity->id) }}">
                            Download CV
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

<!-- right column -->
<div class="col-md-3 main-content--small">
    <div class="row">
        <div class="col-12">
            @include('admin.model.actions-section', ['entity' => $entity ?? null])
        </div>
    </div>
</div>
@section('scripts')
    <script>
        $(document).ready(function () {
            $('#download_cv').click(function() {
                console.log('ads')
                // console.log("ads", $('#download_cv_form'))
                $('#download_cv_form').submit()
            })
        })
    </script>
@endsection
