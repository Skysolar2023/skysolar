<!-- left column -->
<div class="col-md-9 main-content--large">
    <div class="row">
        <div class="col-12 card card-default main-card">
            <div class="card-body">
                @include('admin.model.inputs.text', ['entity' => $entity ?? null, 'field' => 'title'])

                @include('admin.model.inputs.textarea', ['entity' => $entity ?? null, 'field' => 'content'])
            </div>
        </div>
        <div class="col-12 card card-default main-card">
            @include('admin.model.seo-fields', ['entity' => $entity ?? null])
        </div>
    </div>
</div>

<!-- right column -->
<div class="col-md-3 main-content--small">
    <div class="row">
        <div class="col-12">
            @include('admin.model.actions-section', ['entity' => $entity ?? null])
        </div>
    </div>
</div>
