document.addEventListener('DOMContentLoaded', () => {
let mobileHeaderMenu = document.querySelector('.mobile-header-menu');
let disNone = document.querySelector('.dis-none');
let mobileMenu = document.querySelector('.mobile-menu');

mobileHeaderMenu.onclick = () => {
    mobileHeaderMenu.style.display = 'none';
    disNone.style.display = 'block';
    mobileMenu.style.display = 'flex';
}

disNone.onclick = () => {
    disNone.style.display = 'none';
    mobileHeaderMenu.style.display = 'block';
    mobileMenu.style.display = 'none';
}
});
