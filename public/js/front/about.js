gsap.registerPlugin(ScrollTrigger);

gsap.to(".about-sec-1-text", {
    scrollTrigger: ".about-sec-1-text ",
    y: 0,
    duration: .8,
})

gsap.to(".about-sec-1-img", {
    scrollTrigger: ".about-sec-1-img",
    y: -110,
    duration: .8,
})

gsap.to(".about-sec-1-text-s", {
    scrollTrigger: ".about-sec-1-text-s",
    y: 0,
    duration: .8,
})

gsap.to(".about-sec-2-h4", {
    scrollTrigger: ".about-sec-2-h4",
    y: 0,
    duration: .8,
})

gsap.to(".about-sec-2-h4-2", {
    scrollTrigger: ".about-sec-2-h4-2",
    y: 0,
    duration: .8,
})

gsap.to(".about-yellow-h4", {
    scrollTrigger: ".about-yellow-h4",
    y: 0,
    duration: .8,
})

gsap.to(".about-yellow-h4-2", {
    scrollTrigger: ".about-yellow-h4-2",
    y: 0,
    duration: .8,
})

gsap.to(".about-sec-3-h4", {
    scrollTrigger: ".about-sec-3-h4",
    y: 0,
    duration: .8,
})

gsap.to(".about-sec-4-h4", {
    scrollTrigger: ".about-sec-4-h4",
    y: 0,
    duration: .8,
})

gsap.to(".about-sec-4-h4-2", {
    scrollTrigger: ".about-sec-4-h4-2",
    y: 0,
    duration: .8,
})

gsap.to(".about-sec-4-h4-3", {
    scrollTrigger: ".about-sec-4-h4-3",
    y: 0,
    duration: .8,
})

gsap.to(".about-sec_8_h4", {
    scrollTrigger: ".about-sec_8_h4",
    y: 0,
    duration: .8,
})

gsap.to(".about-sec-6-h4", {
    scrollTrigger: ".about-sec-6-h4",
    y: 0,
    duration: .8,
})

gsap.to(".about-sec-6-h4-2", {
    scrollTrigger: ".about-sec-6-h4-2",
    y: 0,
    duration: .8,
})

let s6 = new Swiper('.about-swiper-container', {
    slidesPerView: 'auto',
    slideToClickedSlide: true,
});
const swiperSlides = document.getElementsByClassName('about-swiper-slide');
s6.on('slideChange', function () {
    const otherSlides = swiperSlides
    for (let index = 0; index < swiperSlides.length; index++) {
        const element = swiperSlides[index];
    }
});
