gsap.registerPlugin(ScrollTrigger);

gsap.to(".services-sec-1-text", {
    scrollTrigger: ".services-sec-1-text ",
    y: 0,
    duration: .8,
})

gsap.to(".services-sec-1-img", {
    scrollTrigger: ".services-sec-1-img",
    y: -110,
    duration: .8,
})

gsap.to(".services-sec-1-text-s", {
    scrollTrigger: ".services-sec-1-text-s",
    y: 0,
    duration: .8,
})

gsap.to(".services-sec-2-h3", {
    scrollTrigger: ".services-sec-2-h3",
    y: 0,
    duration: .8,
})

let sections = gsap.utils.toArray(".services-panel");

let scrollTween = gsap.to(sections, {
    xPercent: -100 * (sections.length - 1),
    ease: "none",
    scrollTrigger: {
        trigger: ".services-wrapper",
        pin: true,
        scrub: 0.1,
        start: "top -20%",
        end: "+=4000",
    },
})

gsap.to(".services-sec-3-h3", {
    scrollTrigger: ".services-sec-3-h3",
    y: 0,
    duration: .8,
})

gsap.to(".services-sec-4-h3", {
    scrollTrigger: ".services-sec-4-h3",
    y: 0,
    duration: .8,
})

gsap.to(".services-sec-5-h3", {
    scrollTrigger: ".services-sec-5-h3",
    y: 0,
    duration: .8,
})

gsap.to(".services-sec-5-h3-2", {
    scrollTrigger: ".services-sec-5-h3-2",
    y: 0,
    duration: .8,
})

gsap.to(".services-sec_12_h5", {
    scrollTrigger: ".services-sec_12_h5",
    y: 0,
    duration: .8,
})

gsap.to(".services-sec_12_h5-2", {
    scrollTrigger: ".services-sec_12_h5-2",
    y: 0,
    duration: .8,
})

gsap.to(".services-sec-5-h3-p-cont", {
    scrollTrigger: ".services-sec-5-h3-p-cont",
    y: 0,
    duration: .8,
})

gsap.to(".services-sec_13_h4", {
    scrollTrigger: ".services-sec_13_h4",
    y: 0,
    duration: .8,
})
