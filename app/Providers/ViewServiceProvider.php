<?php

namespace App\Providers;

use App\Services\HelperService;
use App\Services\PagesService;
use App\Services\SettingsService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function ($view) {
            $view->with('settings', app(SettingsService::class));
            $view->with('helper', app(HelperService::class));
            $view->with('contents', app(PagesService::class));
        });
    }
}
