<?php

namespace App\Http\Controllers\Admin;

use App\Models\Attachment;
use App\Models\PageContent;

class PageContentController extends ResourceController
{
    protected $resource = 'page-contents';
    protected $modelName = 'PageContent';

    public function drawFilters($query, $filters)
    {
        switch ($filters) {
            case 'type':
                $query->where('type', $filters['type']);

        }

        return $query;
    }

    protected function storeModel($validated) {
        if ($validated['content_type'] === PageContent::CONTENT_TYPE_VIDEO_URL) {
            $validated['content'] = $validated['video_url'];
        }
        unset($validated['video_url']);

        return $this->model::create($validated);
    }

    protected function updateModel($entity, $validated) {
        if ($validated['content_type'] === PageContent::CONTENT_TYPE_VIDEO_URL) {
            $validated['content'] = $validated['video_url'];
        }
        unset($validated['video_url']);

        return $entity->update($validated);
    }
}
