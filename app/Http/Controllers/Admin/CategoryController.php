<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;

class CategoryController extends ResourceController
{
    protected $resource = 'categories';
    protected $modelName = 'Category';

    public function list()
    {
        return response()->json(['categories' => $this->model::all()]);
    }
}
