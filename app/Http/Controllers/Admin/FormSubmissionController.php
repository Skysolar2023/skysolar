<?php

namespace App\Http\Controllers\Admin;

use App\Models\FormSubmission;
use Illuminate\Support\Facades\Storage;

class FormSubmissionController extends ResourceController
{
    protected $resource = 'form-submissions';

    protected $modelName = 'FormSubmission';

    public function downloadCv($id)
    {
        $formSubmission = FormSubmission::findOrFail($id);
        if (!$formSubmission->attachment) return back()->with(['message' => 'attachment not found!']);

        return Storage::download('public/media/' . $formSubmission->attachment);
    }

    public function drawFilters($query, $filters)
    {
        foreach ($filters as $key => $filter) {
            switch ($key) {
                case 'type':
                    if ($filter) {

                        $query->where($key, $filter);
                    }
            }
        }

        return $query;
    }
}
