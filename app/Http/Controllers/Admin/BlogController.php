<?php

namespace App\Http\Controllers\Admin;

class BlogController extends ResourceController
{
    protected $resource = 'blog';
    protected $modelName = 'Blog';

    protected $drawWith = ['mainImage'];


    protected function storeModel($validated)
    {
        $relations = $validated['relations'];
        unset($validated['relations']);

        $entity = $this->model::create($validated);

        $entity->categories()->sync($relations['categories'] ?? []);

        return $entity;
    }

    public function updateModel($entity, $validated)
    {
        $relations = $validated['relations'];
        unset($validated['relations']);

        $entity->update($validated);

        $entity->categories()->sync($relations['categories'] ?? []);

        return $entity;
    }
}
