<?php

namespace App\Http\Controllers\Admin;

class ExampleController extends ResourceController
{
    protected $resource = 'examples';
    protected $modelName = 'Example';

    protected $drawWith = ['mainImage'];


    protected function storeModel($validated)
    {
        $relations = $validated['relations'];
        unset($validated['relations']);

        $entity = $this->model::create($validated);

        $entity->categories()->sync($relations['categories'] ?? []);
        $entity->attachments()->sync($relations['attachments'] ?? []);

        return $entity;
    }

    public function updateModel($entity, $validated)
    {
        $relations = $validated['relations'];
        unset($validated['relations']);

        $entity->update($validated);

        $entity->categories()->sync($relations['categories'] ?? []);
        $entity->attachments()->sync($relations['attachments'] ?? []);


        return $entity;
    }
}
