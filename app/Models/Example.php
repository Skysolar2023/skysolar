<?php

namespace App\Models;

class Example extends AppModel
{
    protected $table = 'examples';
    protected $casts = ['info' => 'array'];

    const rules = [
        'title' => 'required|string|max:100',
        'description' => 'nullable|string|max:255',
        'slug' => 'required|string|max:100',
        'status' => 'required|string|max:20',
        'info' => 'required|array',
        'info.power' => 'required|string',
        'info.annual_productivity' => 'required|string',
        'info.saving_amount' => 'required|string',
        'meta_title' => 'nullable|string',
        'meta_description' => 'nullable|string',
        'canonical' => 'nullable|string',
        'main_image' => 'nullable|string',
        'relations' => 'required|array',
        'relations.categories' => 'required|array',
    ];

    protected $searchFields = ['title', 'id'];

    public function mainImage(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Attachment::class, 'id', 'main_image');
    }

    public function categories(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(CategoryExample::class, 'example_category','example_id', 'category_id');
    }

    public function attachments(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Attachment::class, 'example_attachment');
    }
}
