<?php

namespace App\Models;

class CategoryExample extends AppModel
{
    protected $table = 'categories_example';
    const rules = [
        'slug' => 'required|unique:categories|string|max:255',
        'status' => 'required|string',
        'name' => 'required|string|max:255',
        'meta_title' => 'string|nullable|max:255',
        'canonical' => 'string|nullable',
        'meta_description' => 'string|nullable',
    ];

    public const updateRules = [
        'slug' => 'required|string|max:255|unique:categories,slug,{{id}}',
    ];

    protected $appends = ['seo'];
    protected $searchFields = ['name', 'id'];

    //    ATTRIBUTES

    public function blogList()
    {
        return $this->belongsToMany(Blog::class, 'blog_category', 'blog_id', 'category_id');
    }

    public function getSeoAttribute(): array
    {
        return [
            'title' => $this->meta_title ?? $this->name,
            'description' => $this->meta_description,
            'canonical' => $this->canonical ?? '/categories/' . $this->slug,
        ];
    }
}
