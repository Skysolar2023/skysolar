<?php

namespace App\Models;

class Car extends AppModel
{
    const rules = [
        'slug' => 'required|unique:cars|string|max:255',
        'status' => 'required|string',
        'title' => 'required|string|max:255',
        'content' => 'string|nullable',
        'price' => 'numeric|nullable',
        'color' => 'string|nullable',
        'meta_title' => 'string|max:255|nullable',
        'canonical' => 'string|nullable',
        'meta_description' => 'string|nullable',
        'main_image' => 'required|numeric',
        'category_id' => 'required|numeric',
        'relations.attachments' => 'nullable|array',
    ];

    public const updateRules = [
        'slug' => 'required|string|max:255|unique:cars,slug,{{id}}',
    ];

    protected $appends = ['publish_url', 'seo'];
    protected $searchFields = ['id', 'title', 'color', 'price'];

    //    ATTRIBUTES
    public function getPublishUrlAttribute(): string
    {
        return $this->publishUrl();
    }

    //    RELATIONS
    public function mainImage(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Attachment::class, 'id', 'main_image');
    }

    public function category(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function publishUrl(): string
    {
        return url('/cars/' . $this->slug);
    }

    public function getSeoAttribute(): array
    {
        return [
            'title' => $this->meta_title ?? $this->title,
            'description' => $this->meta_description,
            'canonical' => $this->canonical ?? explode('?', $this->publishUrl())[0],
        ];
    }
    public function attachments(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Attachment::class, 'car_attachment');
    }
}
