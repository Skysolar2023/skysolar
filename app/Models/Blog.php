<?php

namespace App\Models;

class Blog extends AppModel
{
    protected $table = 'blog';
    const rules = [
        'title' => 'required|string|max:100',
        'description' => 'nullable|string|max:255',
        'slug' => 'required|string|max:100',
        'status' => 'required|string|max:20',
        'content' => 'nullable|string',
        'meta_title' => 'nullable|string',
        'meta_description' => 'nullable|string',
        'canonical' => 'nullable|string',
        'main_image' => 'nullable|string',
        'relations' => 'required|array',
        'relations.categories' => 'required|array',
    ];

    protected $searchFields = ['title', 'id'];

    public function mainImage(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Attachment::class, 'id', 'main_image');
    }

    public function categories(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'blog_category');
    }
}
