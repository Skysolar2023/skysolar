<?php

namespace App\Models;

class PageContent extends AppModel
{
    const TYPE_HOMEPAGE = 'home';
    const CONTENT_TYPE_HTML = 'html';
    const CONTENT_TYPE_VIDEO_URL = 'video_url';

    const rules = [
        'title' => 'nullable|string|max:100',
        'subtitle' => 'nullable|string|max:100',
        'type' => 'string|required|max:20',
        'section_id' => 'string|required|max:20',
        'content_type' => 'string|required|max:20',
        'content' => 'required_if:content_type,html|string',
        'video_url' => 'required_if:content_type,video_url|nullable|string',
        'image' => 'nullable|numeric',
        'active' => 'boolean|nullable',
    ];

    protected $searchFields = ['title', 'subtitle', 'id', 'section_id'];

    public function mainImage(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Attachment::class, 'id', 'image');
    }
}
