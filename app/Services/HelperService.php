<?php
namespace App\Services;

use Cocur\Slugify\Slugify;

class HelperService {

    public function filterText($text) {
        if(!is_string($text)) {
            return $text;
        }

        return htmlspecialchars_decode(trim(preg_replace('/\s+/', '', $text)));
    }

    public function isMobile() {
        return isset($_SERVER["HTTP_USER_AGENT"]) ? preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]) : false;
    }

    public function slugify($text) {
        $slugify = new Slugify();
        $slugify->addRule('&', 'and');
        $slugify->addRule('.', '-');
        $slugify->addRule('/', '-');
        $slugify->addRule('\\', '-');
        return $slugify->slugify($text);
    }
}
