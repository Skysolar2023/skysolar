const mix = require('laravel-mix')
require('dotenv').config()

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// ADMIN
mix.js('resources/js/admin/admin.js', 'public/dist/admin/js')
    .vue()
    .sass('resources/sass/admin/app.scss', 'public/dist/admin/css/main.css')

// FRONT
mix.options({
    processCssUrls: false
});


mix.js(`resources/js/front/main.js`, 'public/dist/front/js')
mix.js(`resources/js/front/front.js`, 'public/dist/front/js').vue()
mix.sass(`resources/sass/front/style.scss`, 'public/dist/front/css');

mix.webpackConfig({
    stats: {
        children: true,
    },
});
