<?php

return [
    /*
    |--------------------------------------------------------------------------
    | RECAPTCHA CONFIG
    |-------------------------------------------------------------------
    |
    */

    'recaptcha_site_key' => env('RECAPTCHA_SITE_KEY'),
    'recaptcha_secret_key' => env('RECAPTCHA_SECRET_KEY'),
    'recaptcha_site' => env('RECAPTCHA_SITE'),

];
