<?php

$env = [
    'sandbox' => [
        'api_key'             => env('SHIPPING_SANDBOX_API_KEY', ''),
        'authentication_code' => env('SHIPPING_SANDBOX_AUTHENTICATION_CODE', ''),
    ],
    'live' => [
        'api_key'             => env('SHIPPING_LIVE_API_KEY', ''),
        'authentication_code' => env('SHIPPING_LIVE_AUTHENTICATION_CODE', ''),
    ],
];

return array_merge([
    'mode'    => env('SHIPPING_MODE', 'sandbox'),
], $env[env('SHIPPING_MODE', 'sandbox')]);
