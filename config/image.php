<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',
    'sizes' => [
        'large' => ['width' => 600, 'height' => 800],
        'medium' => ['width' => 300, 'height' => 400],
        'small' => ['width' => 150, 'height' => 200]
    ],

];
